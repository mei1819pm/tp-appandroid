package com.example.tp_appandroid.other_class;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.tp_appandroid.R;

public class NoConnection extends AppCompatActivity {

    private CheckConnection checkConnection;
    private Button btnTryConnetion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);

        btnTryConnetion = findViewById(R.id.btnTryConnection);

        btnTryConnetion.setOnClickListener(v-> {
            checkConnection = new CheckConnection(this);
            if(checkConnection.isConnected()) {
                this.finish();
            } else {
                Toast.makeText(this, "Por favor volte a tentar", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
