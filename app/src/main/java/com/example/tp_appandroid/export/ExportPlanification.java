package com.example.tp_appandroid.export;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.tp_appandroid.R;


import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExportPlanification extends AppCompatActivity {

    private static final int REQUEST_CODE = 2;

    private Context thisContext;

    private SharedPreferences userSession;
    private int idUser;

    private RadioGroup radioGroup;
    private RadioButton radioButtonSelect;
    private Button btnExport;
    private ProgressBar loadingExportPlanification;

    private int auxIdDiscipline;
    private int auxSchoolYear;

    private int exportTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_planification);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = ExportPlanification.this;

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        Intent getIntent = getIntent();
        auxIdDiscipline = getIntent.getIntExtra("idDiscipline", 0);
        auxSchoolYear  = getIntent.getIntExtra("idSchoolYear",1);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup_exportPlanification);
        btnExport = (Button) findViewById(R.id.btnExportPlanification);
        loadingExportPlanification = findViewById(R.id.loading_exportPlanification);


        btnExport.setOnClickListener(v -> {
            int radioId = radioGroup.getCheckedRadioButtonId();
            radioButtonSelect = findViewById(radioId);

            switch (radioButtonSelect.getText().toString()) {
                case "Texto Simples":
                    exportTo = 0;
                    break;

                case "Excel":
                    exportTo = 1;
                    break;

                default:
                    exportTo = 0;
                    break;
            }

            CheckPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_CODE);
        });
    }

    //Voltar Para Tras
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void CheckPermission(String permissionWrite, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permissionWrite) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permissionWrite}, requestCode);

        } else {
            switch (exportTo) {
                case 0:
                    new AsynExportToPlainText().execute(idUser, auxSchoolYear, auxIdDiscipline);
                    break;

                case 1:
                    new AsynExportToExcel().execute(idUser, auxSchoolYear, auxIdDiscipline);
                    break;

                default:
                    new AsynExportToPlainText().execute(auxIdDiscipline);
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    switch (exportTo) {
                        case 0:
                            new AsynExportToPlainText().execute(idUser, auxSchoolYear, auxIdDiscipline);
                            break;

                        case 1:
                            new AsynExportToExcel().execute(idUser, auxSchoolYear, auxIdDiscipline);
                            break;

                        default:
                            new AsynExportToPlainText().execute(auxIdDiscipline);
                            break;
                    }

                } else {
                    Toast.makeText(this, R.string.other_noPermission, Toast.LENGTH_LONG).show();
                }
        }
    }

    private class AsynExportToExcel extends ExportToExcel {
        @Override
        protected void onPreExecute() {
            loadingExportPlanification.setVisibility(View.VISIBLE);
            btnExport.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result == true) {
                Toast.makeText(thisContext, R.string.success_exportExcel, Toast.LENGTH_LONG).show();
                loadingExportPlanification.setVisibility(View.GONE);
                btnExport.setVisibility(View.VISIBLE);
            } else {
                Toast.makeText(thisContext, R.string.error_Export, Toast.LENGTH_LONG).show();
                loadingExportPlanification.setVisibility(View.GONE);
                btnExport.setVisibility(View.VISIBLE);
            }

        }
    }

    private class AsynExportToPlainText extends ExportToPlainText {
        @Override
        protected void onPreExecute() {
            loadingExportPlanification.setVisibility(View.VISIBLE);
            btnExport.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result == true) {
                Toast.makeText(thisContext, R.string.success_exportTxt, Toast.LENGTH_LONG).show();
                loadingExportPlanification.setVisibility(View.GONE);
                btnExport.setVisibility(View.VISIBLE);
            } else {
                Toast.makeText(thisContext, R.string.error_Export, Toast.LENGTH_LONG).show();
                loadingExportPlanification.setVisibility(View.GONE);
                btnExport.setVisibility(View.VISIBLE);
            }

        }
    }
}
