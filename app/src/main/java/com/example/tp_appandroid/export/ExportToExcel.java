package com.example.tp_appandroid.export;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.ClassContentModel;
import com.example.tp_appandroid.models.ClassMaterialModel;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.models.ContactHourModel;
import com.example.tp_appandroid.models.DisciplineModel;
import com.example.tp_appandroid.models.JobDeliveryModel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

abstract class ExportToExcel extends AsyncTask<Integer, Void, Boolean> {

    @Override
    final protected Boolean doInBackground(Integer... parameters) {


        String auxConcat = "";
        List<DisciplineModel> planifications = new ArrayList<>();

        String path;
        File dir;
        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
            Log.e("Failed", "Storage not available or read only");
            return false;
        }
        boolean success = false;

        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Export/";
        dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<DisciplineModel>> call = service.getPlanification(parameters[0],parameters[1], parameters[2]);
        try {
            planifications = call.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (DisciplineModel auxDiscipline : planifications) {
            int rowNumber = 0;
            //New Workbook
            Workbook wb = new HSSFWorkbook();
            //Cell style for header row
            CellStyle cs = wb.createCellStyle();
            cs.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
            cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);

            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);

            Cell c = null;

            //New Sheet
            Sheet sheet1 = null;
            sheet1 = wb.createSheet("Planificação");

            Row row = null;

            row = sheet1.createRow(rowNumber);

            c = row.createCell(0);
            c.setCellValue(auxDiscipline.getAcronym() + " - Ano Letivo " + auxDiscipline.getSchoolYear());
            c.setCellStyle(cellStyle);

            rowNumber += 2;

            row = sheet1.createRow(rowNumber);
            c = row.createCell(0);
            c.setCellValue("Horário de Atendimento");
            c.setCellStyle(cellStyle);

            rowNumber +=1;

            row = sheet1.createRow(rowNumber);
            c = row.createCell(0);
            c.setCellValue("Dia");
            c.setCellStyle(cs);
            c = row.createCell(1);
            c.setCellValue("Hora");
            c.setCellStyle(cs);
            rowNumber +=1;
            row = sheet1.createRow(rowNumber );
            c = row.createCell(0);
            c.setCellValue(auxDiscipline.getDayAttendace());
            c.setCellStyle(cellStyle);
            c = row.createCell(1);
            c.setCellValue(auxDiscipline.getTimeAttendace());
            c.setCellStyle(cellStyle);

            rowNumber +=2;

            if(auxDiscipline.getClasses().size() != 0) {

                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Aulas");
                c.setCellStyle(cellStyle);

                rowNumber += 1;

                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Número da Aula");
                c.setCellStyle(cs);
                c = row.createCell(1);
                c.setCellValue("Data");
                c.setCellStyle(cs);
                c = row.createCell(2);
                c.setCellValue("Inicio da Aula");
                c.setCellStyle(cs);
                c = row.createCell(3);
                c.setCellValue("Fim da Aula");
                c.setCellStyle(cs);
                c = row.createCell(4);
                c.setCellValue("Sala");
                c.setCellStyle(cs);
                c = row.createCell(5);
                c.setCellValue("Tipo de Aula");
                c.setCellStyle(cs);
                c = row.createCell(6);
                c.setCellValue("Conteúdo da Aula");
                c.setCellStyle(cs);
                c = row.createCell(7);
                c.setCellValue("Ficheiros a entregar");
                c.setCellStyle(cs);

                rowNumber += 1;

                for (ClassModel auxClass : auxDiscipline.getClasses()) {
                    row = sheet1.createRow(rowNumber);

                    c = row.createCell(0);
                    c.setCellValue(auxClass.getNumber());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(1);
                    c.setCellValue(auxClass.getDay());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(2);
                    c.setCellValue(auxClass.getStart());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(3);
                    c.setCellValue(auxClass.getEnd());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(4);
                    c.setCellValue(auxClass.getClassRoom());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(5);
                    c.setCellValue(auxClass.getTypeClass());
                    c.setCellStyle(cellStyle);

                    if (auxClass.getClassContent().size() != 0) {
                        auxConcat = "";
                        for (ClassContentModel auxContent : auxClass.getClassContent()) {
                            auxConcat = auxConcat.concat(auxContent.getDescription() + "; ");
                        }
                        c = row.createCell(6);
                        c.setCellValue(auxConcat);
                        c.setCellStyle(cellStyle);
                    }

                    if (auxClass.getClassMaterial().size() != 0) {
                        auxConcat = "";
                        for (ClassMaterialModel auxMaterial : auxClass.getClassMaterial()) {
                            auxConcat = auxConcat.concat(auxMaterial.getDescription() + "; ");
                        }
                        c = row.createCell(7);
                        c.setCellValue(auxConcat);
                        c.setCellStyle(cellStyle);
                    }

                    rowNumber ++;
                }

            } else {
                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Não existem ainda aulas agendadas");
                c.setCellStyle(cs);
            }

            rowNumber +=2;

            if(auxDiscipline.getJobDeliveries().size() != 0) {

                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Entregas");
                c.setCellStyle(cellStyle);

                rowNumber += 1;

                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Descrição da Entrega");
                c.setCellStyle(cs);
                c = row.createCell(1);
                c.setCellValue("Data");
                c.setCellStyle(cs);
                c = row.createCell(2);
                c.setCellValue("Hora Limite");
                c.setCellStyle(cs);
                c = row.createCell(3);
                c.setCellValue("Local da Entrega");
                c.setCellStyle(cs);

                rowNumber += 1;

                for (JobDeliveryModel auxJobDelivery : auxDiscipline.getJobDeliveries()) {
                    row = sheet1.createRow(rowNumber);

                    c = row.createCell(0);
                    c.setCellValue(auxJobDelivery.getDescription());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(1);
                    c.setCellValue(auxJobDelivery.getDay());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(2);
                    c.setCellValue(auxJobDelivery.getMaxTime());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(3);
                    c.setCellValue(auxJobDelivery.getDeliveryLocation());
                    c.setCellStyle(cellStyle);

                }

            } else {
                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Não existem ainda entregas agendadas");
                c.setCellStyle(cs);
            }

            rowNumber +=2;

            if(auxDiscipline.getJobDeliveries().size() != 0) {

                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Apoios");
                c.setCellStyle(cellStyle);

                rowNumber += 1;

                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Aluno");
                c.setCellStyle(cs);
                c = row.createCell(1);
                c.setCellValue("Descrição do Apoio");
                c.setCellStyle(cs);
                c = row.createCell(2);
                c.setCellValue("Local");
                c.setCellStyle(cs);
                c = row.createCell(3);
                c.setCellValue("Data");
                c.setCellStyle(cs);
                c = row.createCell(4);
                c.setCellValue("Hora");
                c.setCellStyle(cs);

                rowNumber += 1;

                for (ContactHourModel auxContactHour : auxDiscipline.getContactHours()) {
                    row = sheet1.createRow(rowNumber);

                    c = row.createCell(0);
                    c.setCellValue(auxContactHour.getNameStudent());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(1);
                    c.setCellValue(auxContactHour.getDescription());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(2);
                    c.setCellValue(auxContactHour.getAppOrRoom());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(3);
                    c.setCellValue(auxContactHour.getDay());
                    c.setCellStyle(cellStyle);
                    c = row.createCell(4);
                    c.setCellValue(auxContactHour.getHour());
                    c.setCellStyle(cellStyle);
                }

            } else {
                row = sheet1.createRow(rowNumber);
                c = row.createCell(0);
                c.setCellValue("Não existem ainda apoios agendados");
                c.setCellStyle(cs);
            }

            sheet1.setColumnWidth(0, 5000);
            sheet1.setColumnWidth(1, 4000);
            sheet1.setColumnWidth(2, 3500);
            sheet1.setColumnWidth(3, 3500);
            sheet1.setColumnWidth(4, 3000);
            sheet1.setColumnWidth(5, 3000);
            sheet1.setColumnWidth(6, 10000);
            sheet1.setColumnWidth(7, 10000);

            File file = new File(dir, auxDiscipline.getAcronym() + "_" + auxDiscipline.getSchoolYear() + ".xls");
            FileOutputStream os = null;

            try {
                os = new FileOutputStream(file);
                wb.write(os);
                Log.w("FileUtils", "Writing file" + file);
                success = true;
            } catch (IOException e) {
                Log.w("FileUtils", "Error writing " + file, e);
            } catch (Exception e) {
                Log.w("FileUtils", "Failed to save file", e);
            } finally {
                try {
                    if (null != os)
                        os.close();
                } catch (Exception ex) {
                }
            }
        }
        return success;

    }

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }
}
