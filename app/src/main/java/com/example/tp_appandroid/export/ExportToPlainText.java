package com.example.tp_appandroid.export;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.ClassContentModel;
import com.example.tp_appandroid.models.ClassMaterialModel;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.models.ContactHourModel;
import com.example.tp_appandroid.models.DisciplineModel;
import com.example.tp_appandroid.models.JobDeliveryModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;


abstract class ExportToPlainText extends AsyncTask<Integer, Void, Boolean> {

    @Override
    final protected Boolean doInBackground(Integer... parameters) {

        List<DisciplineModel> planifications = new ArrayList<>();
        String path;
        File dir;
        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
            Log.e("Failed", "Storage not available or read only");
            return false;
        }
        boolean success = false;

        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Export/";
        dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<DisciplineModel>> call = service.getPlanification(parameters[0],parameters[1], parameters[2]);
        try {
            planifications = call.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (DisciplineModel auxDiscipline : planifications) {

            StringBuilder disciplinePlanification = new StringBuilder(auxDiscipline.getAcronym() + " - Ano Letivo " + auxDiscipline.getSchoolYear() + "\n\n\n\n");
            disciplinePlanification.append("\n\n");

            disciplinePlanification.append("Horário de Atendimento");
            disciplinePlanification.append("\n");
            disciplinePlanification.append("Dia: " + auxDiscipline.getDayAttendace() + "\n");
            disciplinePlanification.append("Hora: " + auxDiscipline.getTimeAttendace());
            disciplinePlanification.append("\n\n\n");

            if(auxDiscipline.getClasses().size() != 0) {

                disciplinePlanification.append("Aulas");
                disciplinePlanification.append("\n\n");

                for (ClassModel auxClass : auxDiscipline.getClasses()) {
                    disciplinePlanification.append("Número da Aula: " + auxClass.getNumber() + "\n");
                        disciplinePlanification.append("Data: " + auxClass.getDay() + "\n");
                    disciplinePlanification.append("Inicio da Aula: " + auxClass.getStart() + "\n");
                    disciplinePlanification.append("Fim da Aula: " + auxClass.getEnd() + "\n");
                    disciplinePlanification.append("Sala: " + auxClass.getClassRoom() + "\n");
                    disciplinePlanification.append("Tipo de aula: " + auxClass.getTypeClass() + "\n");

                    if (auxClass.getClassContent().size() != 0) {
                        disciplinePlanification.append("Conteúdo da Aula:\n");
                        for (ClassContentModel auxContent : auxClass.getClassContent()) {
                            disciplinePlanification.append("-" + auxContent.getDescription() + "\n");
                        }
                    }

                    if (auxClass.getClassMaterial().size() != 0) {
                        disciplinePlanification.append("Ficheiros a entregar:\n");
                        for (ClassMaterialModel auxMaterial : auxClass.getClassMaterial()) {
                            disciplinePlanification.append("-" + auxMaterial.getDescription() + "\n");
                        }
                    }

                    disciplinePlanification.append("\n");
                }

            } else {
                disciplinePlanification.append("Não existem ainda aulas agendadas");
            }

            disciplinePlanification.append("\n\n\n");

            if(auxDiscipline.getJobDeliveries().size() != 0) {

                disciplinePlanification.append("Entregas");
                disciplinePlanification.append("\n\n");

                for (JobDeliveryModel auxJobDelivery : auxDiscipline.getJobDeliveries()) {
                    disciplinePlanification.append("Descrição da Entrega: " + auxJobDelivery.getDescription() + "\n");
                    disciplinePlanification.append("Data: " + auxJobDelivery.getDay() + "\n");
                    disciplinePlanification.append("Hora Limite: " + auxJobDelivery.getMaxTime() + "\n");
                    disciplinePlanification.append("Local da Entrega: " + auxJobDelivery.getDeliveryLocation() + "\n");
                }

            } else {
                disciplinePlanification.append("Não existem ainda entregas agendadas");
            }

            disciplinePlanification.append("\n\n\n");

            if(auxDiscipline.getJobDeliveries().size() != 0) {

                disciplinePlanification.append("Apoios");
                disciplinePlanification.append("\n\n");

                for (ContactHourModel auxContactHour : auxDiscipline.getContactHours()) {
                    disciplinePlanification.append("Aluno: " + auxContactHour.getNameStudent() + "\n");
                    disciplinePlanification.append("Descrição do Apoio: " + auxContactHour.getDescription() + "\n");
                    disciplinePlanification.append("Local: " + auxContactHour.getAppOrRoom() + "\n");
                    disciplinePlanification.append("Data: " + auxContactHour.getDay() + "\n");
                    disciplinePlanification.append("Hora: " + auxContactHour.getHour() + "\n");
                }

            } else {
                disciplinePlanification.append("Não existem ainda apoios agendados");
            }

                File file = new File(dir,  auxDiscipline.getAcronym() + "_" + auxDiscipline.getSchoolYear() + ".txt");
                FileWriter writer = null;

                try {
                    writer = new FileWriter(file);
                    writer.append(disciplinePlanification.toString());

                    Log.w("FileUtils", "Writing file" + file);
                    success = true;
                } catch (IOException e) {
                    Log.w("FileUtils", "Error writing " + file, e);
                } catch (Exception e) {
                    Log.w("FileUtils", "Failed to save file", e);
                } finally {
                    try {
                        if (null != writer) {
                            writer.flush();
                            writer.close();
                        }
                    } catch (Exception ex) {
                    }
                }

        }
        
        return success;
    }

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }
}
