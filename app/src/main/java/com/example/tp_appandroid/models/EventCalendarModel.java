package com.example.tp_appandroid.models;

import java.util.Date;

public class EventCalendarModel {

    private String date;
    private Integer typeEvent;

    public EventCalendarModel() {
        this.date = "";
        this.typeEvent = -1;
    }

    public EventCalendarModel(String date, Integer typeEvent) {
        this.date = date;
        this.typeEvent = typeEvent;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(Integer typeEvent) {
        this.typeEvent = typeEvent;
    }
}
