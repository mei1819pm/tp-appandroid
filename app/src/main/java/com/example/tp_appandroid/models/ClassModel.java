package com.example.tp_appandroid.models;

import java.util.ArrayList;
import java.util.List;

public class ClassModel {
    private Integer id;
    private Integer idDiscipline;
    private String discipline;
    private String course;
    private Integer number;
    private String day;
    private String start;
    private String end;
    private String classRoom;
    private String typeClass;
    private List<ClassContentModel> classContent;
    private List<ClassMaterialModel> classMaterial;

    public ClassModel() {
        this.id = -1;
        this.discipline = "";
        this.idDiscipline = -1;
        this.course = "";
        this.number = -1;
        this.day = "";
        this.start = "";
        this.end = "";
        this.classRoom = "";
        this.typeClass = "";
        this.classContent = new ArrayList<>();
        classMaterial = new ArrayList<>();
    }

    public ClassModel(Integer id, Integer idDiscipline, String discipline, String course, Integer number, String day, String start, String end, String classRoom, String typeClass, List<ClassContentModel> classContent, List<ClassMaterialModel> classMaterial) {
        this.id = id;
        this.idDiscipline = idDiscipline;
        this.discipline = discipline;
        this.course = course;
        this.number = number;
        this.day = day;
        this.start = start;
        this.end = end;
        this.classRoom = classRoom;
        this.typeClass = typeClass;
        this.classContent = classContent;
        this.classMaterial = classMaterial;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getTypeClass() {
        return typeClass;
    }

    public void setTypeClass(String typeClass) {
        this.typeClass = typeClass;
    }

    public List<ClassContentModel> getClassContent() {
        return classContent;
    }

    public void setClassContent(List<ClassContentModel> classContent) {
        this.classContent = classContent;
    }

    public List<ClassMaterialModel> getClassMaterial() {
        return classMaterial;
    }

    public void setClassMaterial(List<ClassMaterialModel> classMaterial) {
        this.classMaterial = classMaterial;
    }

    public Integer getIdDiscipline() {
        return idDiscipline;
    }

    public void setIdDiscipline(Integer idDiscipline) {
        this.idDiscipline = idDiscipline;
    }
}
