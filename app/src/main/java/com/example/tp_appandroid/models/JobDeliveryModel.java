package com.example.tp_appandroid.models;

public class JobDeliveryModel {

    private Integer id;
    private String discipline;
    private String course;
    private String description;
    private String day;
    private String maxTime;
    private String deliveryLocation;
    private Integer idDiscipline;

    public JobDeliveryModel() {
        this.id = -1;
        this.discipline = "";
        this.course = "";
        this.description = "";
        this.day = "";
        this.maxTime = "";
        this.deliveryLocation = "";
        this.idDiscipline = -1;
    }

    public JobDeliveryModel(Integer id, String discipline, String course, String description, String day, String maxTime, String deliveryLocation, int idDiscipline) {
        this.id = id;
        this.discipline = discipline;
        this.course = course;
        this.description = description;
        this.day = day;
        this.maxTime = maxTime;
        this.deliveryLocation = deliveryLocation;
        this.idDiscipline = idDiscipline;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    public Integer getIdDiscipline() {
        return idDiscipline;
    }

    public void setIdDiscipline(Integer idDiscipline) {
        this.idDiscipline = idDiscipline;
    }
}
