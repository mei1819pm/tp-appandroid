package com.example.tp_appandroid.models;

public class ContactHourModel {

    private int id;
    private String day;
    private String hour;
    private String type;
    private String appOrRoom;
    private String description;
    private String discipline;
    private String course;
    private String nameStudent;
    private int idDiscipline;
    private int idStudent;
    private int idTeacher;

    public ContactHourModel() {
        this.id = -1;
        this.day = "";
        this.hour = "";
        this.type = "";
        this.appOrRoom = "";
        this.description = "";
        this.discipline = "";
        this.course = "";
        this.nameStudent = "";
        this.idDiscipline = -1;
        this.idStudent = -1;
        this.idTeacher = -1;
    }

    public ContactHourModel(int id, String day, String hour, String type, String appOrRoom, String description, String discipline, String course, String nameStudent, int idDiscipline, int idStudent, int idTeacher) {
        this.id = id;
        this.day = day;
        this.hour = hour;
        this.type = type;
        this.appOrRoom = appOrRoom;
        this.description = description;
        this.discipline = discipline;
        this.course = course;
        this.nameStudent = nameStudent;
        this.idDiscipline = idDiscipline;
        this.idStudent = idStudent;
        this.idTeacher = idTeacher;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAppOrRoom() {
        return appOrRoom;
    }

    public void setAppOrRoom(String appOrRoom) {
        this.appOrRoom = appOrRoom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getNameStudent() {
        return nameStudent;
    }

    public void setNameStudent(String nameStudent) {
        this.nameStudent = nameStudent;
    }

    public int getIdDiscipline() {
        return idDiscipline;
    }

    public void setIdDiscipline(int idDiscipline) {
        this.idDiscipline = idDiscipline;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(int idTeacher) {
        this.idTeacher = idTeacher;
    }
}
