package com.example.tp_appandroid.models;

import java.util.ArrayList;
import java.util.List;

public class DisciplineModel {

    private String name;
    private String acronym;
    private int idCourse;
    private int idSemester;
    private int idSchoolYear;
    private int idUser;
    private int id;
    private String schoolYear;
    private String timeAttendace;
    private String dayAttendace;
    private String nameCourse;
    private String semester;
    private List<ClassModel> classes;
    private List<ContactHourModel> contactHours;
    private List<JobDeliveryModel> jobDeliveries;

    public DisciplineModel() {
        this.name ="";
        this.acronym = "";
        this.schoolYear = "";
        this.idCourse = -1;
        this.idSemester = -1;
        this.idSchoolYear = -1;
        this.idUser = -1;
        this.id=-1;
        this.timeAttendace = "";
        this.dayAttendace = "";
        this.nameCourse = "";
        this.semester = "";
        this.classes = new ArrayList<>();
        this.contactHours = new ArrayList<>();
        this.jobDeliveries = new ArrayList<>();
    }

    public DisciplineModel(String name) {
        this.name = name;
        this.acronym = "";
        this.schoolYear = "";
        this.idCourse = -1;
        this.idSemester = -1;
        this.idSchoolYear = -1;
        this.idUser = -1;
        this.id=-1;
        this.timeAttendace = "";
        this.dayAttendace = "";
        this.nameCourse = "";
        this.semester = "";
        this.classes = new ArrayList<>();
        this.contactHours = new ArrayList<>();
        this.jobDeliveries = new ArrayList<>();
    }

    public DisciplineModel(String name, String nameDiscipline, String schoolYear, int idCourse, int idSemester,int idSchoolYear, int idUser, int id,  String timeAttendace, String dayAttendace, String semester, String nameCourse, List<ClassModel> classes, List<ContactHourModel> contactHours, List<JobDeliveryModel> jobDeliveries) {
        this.name = name;
        this.acronym = nameDiscipline;
        this.schoolYear = schoolYear;
        this.idCourse = idCourse;
        this.idSemester = idSemester;
        this.idSchoolYear = idSchoolYear;
        this.idUser = idUser;
        this.id = id;
        this.timeAttendace = timeAttendace;
        this.dayAttendace = dayAttendace;
        this.nameCourse = nameCourse;
        this.semester = semester;
        this.classes = classes;
        this.contactHours = contactHours;
        this.jobDeliveries = jobDeliveries;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public List<ClassModel> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassModel> classes) {
        this.classes = classes;
    }

    public String getTimeAttendace() {
        return timeAttendace;
    }

    public void setTimeAttendace(String timeAttendace) {
        this.timeAttendace = timeAttendace;
    }

    public String getDayAttendace() {
        return dayAttendace;
    }

    public void setDayAttendace(String dayAttendace) {
        this.dayAttendace = dayAttendace;
    }

    public List<ContactHourModel> getContactHours() {
        return contactHours;
    }

    public void setContactHours(List<ContactHourModel> contactHours) {
        this.contactHours = contactHours;
    }

    public List<JobDeliveryModel> getJobDeliveries() {
        return jobDeliveries;
    }

    public void setJobDeliveries(List<JobDeliveryModel> jobDeliveries) {
        this.jobDeliveries = jobDeliveries;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }

    public int getIdSemester() {
        return idSemester;
    }

    public void setIdSemester(int idSemester) {
        this.idSemester = idSemester;
    }

    public int getIdSchoolYear() {
        return idSchoolYear;
    }

    public void setIdSchoolYear(int idSchoolYear) {
        this.idSchoolYear = idSchoolYear;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
