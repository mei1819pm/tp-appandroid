package com.example.tp_appandroid.models;

public class SchoolYearModel {

    private Integer id;
    private String description;
    private Integer current;

    public SchoolYearModel() {
        this.id = -1;
        this.description = "";
        this.current = -1;
    }

    public SchoolYearModel(String description) {
        this.id = -1;
        this.description = description;
        this.current = -1;
    }

    public SchoolYearModel(Integer id, String description, Integer current) {
        this.id = id;
        this.description = description;
        this.current = current;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }
}
