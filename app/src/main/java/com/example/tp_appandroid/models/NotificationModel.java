package com.example.tp_appandroid.models;

public class NotificationModel {

    private int id;
    private String title;
    private String discipline;
    private String hour;
    private String roomOrMethod;
    private String student;
    private String day;

    public NotificationModel() {
        this.id = -1;
        this.title = "";
        this.discipline = "";
        this.hour = "";
        this.roomOrMethod = "";
        this.student = "";
        this.day = "";
    }

    public NotificationModel(int id, String title, String discipline, String hour, String roomOrMethod, String student, String day) {
        this.id = id;
        this.title = title;
        this.discipline = discipline;
        this.hour = hour;
        this.roomOrMethod = roomOrMethod;
        this.student = student;
        this.day = day;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getRoomOrMethod() {
        return roomOrMethod;
    }

    public void setRoomOrMethod(String roomOrMethod) {
        this.roomOrMethod = roomOrMethod;
    }
}
