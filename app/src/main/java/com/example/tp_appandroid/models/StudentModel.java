package com.example.tp_appandroid.models;

public class StudentModel {

    private Integer id;
    private Integer number;
    private String name;
    private String email;
    private String password;
    private Integer delegate;
    private Integer idDiscipline;
    private Integer idTeacher;

    public StudentModel() {
        this.id = -1;
        this.number = -1;
        this.name = "";
        this.email = "";
        this.password = "";
        this.delegate = -1;
        this.idDiscipline = -1;
        this.idTeacher = -1;
    }

    public StudentModel(Integer id, Integer number, String name, String email, String password, Integer delegate, Integer idDiscipline, Integer idTeacher) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.email = email;
        this.password = password;
        this.delegate = delegate;
        this.idDiscipline = idDiscipline;
        this.idTeacher = idTeacher;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getDelegate() {
        return delegate;
    }

    public void setDelegate(Integer delegate) {
        this.delegate = delegate;
    }

    public Integer getIdDiscipline() {
        return idDiscipline;
    }

    public void setIdDiscipline(Integer idDiscipline) {
        this.idDiscipline = idDiscipline;
    }

    public Integer getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(Integer idTeacher) {
        this.idTeacher = idTeacher;
    }
}
