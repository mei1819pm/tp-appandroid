package com.example.tp_appandroid.models;

public class EmailModel {

    private Integer id;
    private Integer idDiscipline;
    private String email;
    private String password;
    private Integer sendTo;
    private String emailAdministrative;
    private String subject;
    private String body;

    public EmailModel() {
        this.id = -1;
        this.idDiscipline = -1;
        this.email = "";
        this.password = "";
        this.sendTo = -1;
        this.emailAdministrative = "";
        this.subject = "";
        this.body = "";
    }

    public EmailModel(Integer id, Integer idDiscipline, String email, String password, Integer sendTo, String emailAdministrative, String subject, String body) {
        this.id = id;
        this.idDiscipline = idDiscipline;
        this.email = email;
        this.password = password;
        this.sendTo = sendTo;
        this.emailAdministrative = emailAdministrative;
        this.subject = subject;
        this.body = body;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDiscipline() {
        return idDiscipline;
    }

    public void setIdDiscipline(Integer idDiscipline) {
        this.idDiscipline = idDiscipline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSendTo() {
        return sendTo;
    }

    public void setSendTo(Integer sendTo) {
        this.sendTo = sendTo;
    }

    public String getEmailAdministrative() {
        return emailAdministrative;
    }

    public void setEmailAdministrative(String emailAdministrative) {
        this.emailAdministrative = emailAdministrative;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
