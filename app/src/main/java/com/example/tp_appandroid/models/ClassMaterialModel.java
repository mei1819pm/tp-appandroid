package com.example.tp_appandroid.models;

public class ClassMaterialModel {

    private Integer id;
    private String description;
    private Integer idClass;

    public ClassMaterialModel() {
        this.id = -1;
        this.idClass = -1;
        this.description = "";
    }

    public ClassMaterialModel(Integer id, String description, Integer idClass) {
        this.id = id;
        this.idClass = idClass;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdClass() {
        return idClass;
    }

    public void setIdClass(Integer idClass) {
        this.idClass = idClass;
    }
}
