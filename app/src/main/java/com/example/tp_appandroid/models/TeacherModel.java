package com.example.tp_appandroid.models;

import android.content.Context;

import com.google.gson.Gson;

public class TeacherModel {

    private int id;
    private String name;
    private String email;
    private String password;
    private Boolean login;
    private int notification;
    private String number;
    private String university;
    private String graduated;
    private String descriptionDegree;

    public TeacherModel() {
        this.id = 0;
        this.name = "";
        this.email = "";
        this.password = "";
        this.login = false;
        this.notification = 0;
        this.number = "";
        this.university = "";
        this.graduated = "";
        this.descriptionDegree = "";
    }

    public TeacherModel(int id, String name, String email, String password, Boolean login, int notification, String number, String university, String graduated, String descriptionDegree) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.login = login;
        this.notification = notification;
        this.number = number;
        this.university = university;
        this.graduated = graduated;
        this.descriptionDegree = descriptionDegree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    public int getNotification() {
        return notification;
    }

    public void setNotification(int notification) {
        this.notification = notification;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getGraduated() {
        return graduated;
    }

    public void setGraduated(String graduated) {
        this.graduated = graduated;
    }

    public String getDescriptionDegree() {
        return descriptionDegree;
    }

    public void setDescriptionDegree(String descriptionDegree) {
        this.descriptionDegree = descriptionDegree;
    }
}
