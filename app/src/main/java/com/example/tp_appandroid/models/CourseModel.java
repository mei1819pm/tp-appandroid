package com.example.tp_appandroid.models;

public class CourseModel {

    private int id;
    private String description;
    private String semester;

    public CourseModel() {
        this.id = -1;
        this.description = "";
        this.semester = "";
    }

    public CourseModel(String description) {
        this.id = -1;
        this.description = description;
        this.semester = "";
    }

    public CourseModel(int id, String description, String semester) {
        this.id = id;
        this.description = description;
        this.semester = semester;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }
}
