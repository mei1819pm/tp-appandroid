package com.example.tp_appandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tp_appandroid.other_class.NoConnection;
import com.example.tp_appandroid.today.HomeApp;
import com.example.tp_appandroid.other_class.CheckConnection;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.TeacherModel;
import com.example.tp_appandroid.new_account.NewAccount;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private CheckConnection checkConnection;
    private Button btnLogin;
    private TextView  txtNewAccount, txtErrorLogin;
    private EditText txtEmail, txtPassword;

    private SharedPreferences userSession;

    private TeacherModel teacherResposne = new TeacherModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_main);

        //Ativar serviço de envio de notificações
        Intent intentStartServiceSendNotification = new Intent();

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);

        btnLogin = findViewById(R.id.btnLogin);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtNewAccount = (TextView) findViewById(R.id.txtNewAccount);
        txtErrorLogin = (TextView) findViewById(R.id.txtErrorLogin);

        // Verificar Ligação
        checkConnection = new CheckConnection(this);
        if (!checkConnection.isConnected()) {
            Intent intentNoConnection = new Intent(this, NoConnection.class);
            startActivity(intentNoConnection);
        }

        Intent intentHomeActivity = new Intent(MainActivity.this, HomeApp.class);

        if (userSession.contains("initialized")) {
            intentStartServiceSendNotification.setAction("START_SERVICE_SEND_NOTIFICATION");
            sendBroadcast(intentStartServiceSendNotification);
            startActivity(intentHomeActivity);
        } else {

            txtEmail.addTextChangedListener(CheckInputEmpty);
            txtPassword.addTextChangedListener(CheckInputEmpty);
        }

        btnLogin.setOnClickListener(v -> {
            CheckLogin(intentStartServiceSendNotification, intentHomeActivity);
        });

        txtNewAccount.setOnClickListener(v -> {
            Intent intentNewAccount = new Intent(MainActivity.this, NewAccount.class);
            startActivity(intentNewAccount);
        });

    }

    private TextWatcher CheckInputEmpty = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String usernameInput = txtEmail.getText().toString().trim();
            String passwordInput = txtPassword.getText().toString().trim();

            btnLogin.setEnabled(!usernameInput.isEmpty() && !passwordInput.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        System.exit(0);
    }

    private void CheckLogin(Intent intentService, Intent intentGoToHome) {

        TeacherModel teacherRequest = new TeacherModel();
        teacherRequest.setEmail(txtEmail.getText().toString());
        teacherRequest.setPassword(txtPassword.getText().toString());

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<TeacherModel> cal = service.checkLogin(teacherRequest);
        cal.enqueue(new Callback<TeacherModel>() {
            @Override
            public void onResponse(Call<TeacherModel> call, Response<TeacherModel> response) {
                if(response.code() == 200) {
                    teacherResposne = response.body();

                    if(teacherResposne.getLogin() == true) {
                        SharedPreferences.Editor editor = userSession.edit();
                        editor.putString("nameUser",teacherResposne.getName());
                        editor.putInt("idUser",teacherResposne.getId());
                        editor.putString("emailUser", teacherResposne.getEmail());
                        editor.putBoolean("initialized", true);
                        editor.commit();

                        intentService.setAction("START_SERVICE_SEND_NOTIFICATION");
                        sendBroadcast(intentService);

                        startActivity(intentGoToHome);

                    } else {
                        txtErrorLogin.setText("Correio Eletrónico ou palavra-passe incorretos");
                    }

                }
            }

            @Override
            public void onFailure(Call<TeacherModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
