package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.ContactHourModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapterContactHour extends RecyclerView.Adapter<RecyclerViewAdapterContactHour.ViewHolderContactHour>{

    private Context mContext;
    private List<ContactHourModel> mData;
    private static ClickListener clickListener;

    public RecyclerViewAdapterContactHour(Context mContext, List<ContactHourModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolderContactHour onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_contacthour_discipline, viewGroup, false);
        ViewHolderContactHour viewHolderContactHour = new ViewHolderContactHour(v);

        return viewHolderContactHour;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderContactHour viewHolderContactHour, int i) {

        viewHolderContactHour.student.setText(mData.get(i).getNameStudent());
        viewHolderContactHour.date.setText(mData.get(i).getDay());
        viewHolderContactHour.hour.setText(mData.get(i).getHour());
        viewHolderContactHour.location.setText(mData.get(i).getAppOrRoom());

        viewHolderContactHour.iconMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolderContactHour.iconMenu);
                popupMenu.inflate(R.menu.recyclerview_menu_delete);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.actionDelete_menuRecyclerViewDelete:
                                DeleteContactHour(i);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(RecyclerViewAdapterContactHour.ClickListener clickListener) {
        RecyclerViewAdapterContactHour.clickListener = clickListener;
    }

    public static class ViewHolderContactHour extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView student, date, hour, location, iconMenu;

        public ViewHolderContactHour(@NonNull View itemView) {
            super(itemView);

            student = (TextView) itemView.findViewById(R.id.student_irvContactHourDiscipline);
            date = (TextView) itemView.findViewById(R.id.date_irvContactHourDiscipline);
            hour = (TextView) itemView.findViewById(R.id.hour_irvContactHourDiscipline);
            location = (TextView) itemView.findViewById(R.id.location_irvContactHourDiscipline);
            iconMenu = (TextView) itemView.findViewById(R.id.options_irvContactHourDiscipline);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(v,getAdapterPosition());
            return false;
        }
    }

    public void DeleteContactHour(int position) {

        int idJobDelivery = mData.get(position).getId();

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteContactHour(idJobDelivery);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() > 0) {
                        mData.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, R.string.success_deleteContactHour, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(mContext, R.string.error_deleteContactHour, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemLongClick( View v, int position);
    }
}
