package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.JobDeliveryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapterJobDelivery extends RecyclerView.Adapter<RecyclerViewAdapterJobDelivery.ViewHolderJobDelivery> {

    private Context mContext;
    private List<JobDeliveryModel> mData;
    private static ClickListener clickListener;

    public RecyclerViewAdapterJobDelivery(Context mContext, List<JobDeliveryModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolderJobDelivery onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_jobdelivery_discipline, viewGroup, false);
        ViewHolderJobDelivery viewHolderJobDelivery = new ViewHolderJobDelivery(v);

        return viewHolderJobDelivery;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderJobDelivery viewHolderJobDelivery, int i) {

        viewHolderJobDelivery.description.setText(mData.get(i).getDescription());
        viewHolderJobDelivery.date.setText(mData.get(i).getDay());
        viewHolderJobDelivery.hour.setText(mData.get(i).getMaxTime());
        viewHolderJobDelivery.location.setText(mData.get(i).getDeliveryLocation());

        viewHolderJobDelivery.iconMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolderJobDelivery.iconMenu);
                popupMenu.inflate(R.menu.recyclerview_menu_delete);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.actionDelete_menuRecyclerViewDelete:
                                DeleteJobDelivery(i);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(RecyclerViewAdapterJobDelivery.ClickListener clickListener) {
        RecyclerViewAdapterJobDelivery.clickListener = clickListener;
    }

    public static class ViewHolderJobDelivery extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView description, date, hour, location, iconMenu;

        public ViewHolderJobDelivery(@NonNull View itemView) {
            super(itemView);

            description = (TextView) itemView.findViewById(R.id.description_irvJobDeliveryDiscipline);
            date = (TextView) itemView.findViewById(R.id.date_irvJobDeliveryDiscipline);
            hour = (TextView) itemView.findViewById(R.id.hour_irvJobDeliveryDiscipline);
            location = (TextView) itemView.findViewById(R.id.location_irvJobDeliveryDiscipline);
            iconMenu = (TextView) itemView.findViewById(R.id.options_irvJobDeliveryDiscipline);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(v,getAdapterPosition());
            return false;
        }
    }

    public void DeleteJobDelivery(int position) {

        int idJobDelivery = mData.get(position).getId();

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteJobDelivery(idJobDelivery);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() > 0) {
                        mData.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, R.string.success_deleteJobDelivery, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(mContext, R.string.error_deleteJobDelivery, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemLongClick( View v, int position);
    }

}
