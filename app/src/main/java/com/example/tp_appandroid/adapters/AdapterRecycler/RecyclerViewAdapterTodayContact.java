package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.models.ContactHourModel;

import java.util.List;

public class RecyclerViewAdapterTodayContact extends RecyclerView.Adapter<RecyclerViewAdapterTodayContact.ViewHolderTodayContact> {

    private Context mContext;
    private List<ContactHourModel> mData;

    public RecyclerViewAdapterTodayContact(Context mContext, List<ContactHourModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolderTodayContact onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_today_contact, viewGroup, false);
        ViewHolderTodayContact viewHolderTodayContact = new ViewHolderTodayContact(v);
        return viewHolderTodayContact;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTodayContact viewHolderTodayContact, int i) {
        viewHolderTodayContact.studentItemContact.setText(mData.get(i).getNameStudent());
        viewHolderTodayContact.disciplineItemContact.setText(mData.get(i).getDiscipline());
        viewHolderTodayContact.courseItemContact.setText(mData.get(i).getCourse());
        viewHolderTodayContact.descriptionItemContact.setText(mData.get(i).getDescription());
        viewHolderTodayContact.locationItemContact.setText(mData.get(i).getAppOrRoom());
        viewHolderTodayContact.hourItemContact.setText(mData.get(i).getHour());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolderTodayContact extends RecyclerView.ViewHolder{

        private TextView studentItemContact, disciplineItemContact, courseItemContact, descriptionItemContact, locationItemContact, hourItemContact;

        public ViewHolderTodayContact(@NonNull View itemView) {
            super(itemView);

            studentItemContact = (TextView) itemView.findViewById(R.id.studentItemTodayContact);
            disciplineItemContact = (TextView) itemView.findViewById(R.id.disciplineItemTodayContact);
            courseItemContact = (TextView) itemView.findViewById(R.id.courseItemTodayContact);
            descriptionItemContact = (TextView) itemView.findViewById(R.id.descriptionItemTodayContact);
            locationItemContact = (TextView) itemView.findViewById(R.id.locationItemTodayContact);
            hourItemContact = (TextView) itemView.findViewById(R.id.hourItemTodayContact);
        }
    }
}
