package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.discipline.Class.UpdateClass;
import com.example.tp_appandroid.discipline.Content.ContentManager;
import com.example.tp_appandroid.discipline.Material.MaterialManager;
import com.example.tp_appandroid.models.ClassModel;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapterClass extends RecyclerView.Adapter<RecyclerViewAdapterClass.ViewHolderClass>{

    private Context mContext;
    private int idDiscipline;
    private List<ClassModel> mData;
    private static ClickListener clickListener;

    public RecyclerViewAdapterClass(Context mContext, List<ClassModel> mData, int idDiscipline) {
        this.mContext = mContext;
        this.mData = mData;
        this.idDiscipline = idDiscipline;
    }

    @NonNull
    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recycler_view_adapter_class, viewGroup, false);
        ViewHolderClass viewHolderStudent = new ViewHolderClass(v);

        return viewHolderStudent;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderClass viewHolderClass, int i) {

        viewHolderClass.nClass.setText("Aula nº " + mData.get(i).getNumber() +"");
        viewHolderClass.room.setText(mData.get(i).getClassRoom());
        viewHolderClass.start.setText(mData.get(i).getStart());
        viewHolderClass.end.setText(mData.get(i).getEnd());
        viewHolderClass.day.setText(mData.get(i).getDay());

        viewHolderClass.iconMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolderClass.iconMenu);
                popupMenu.inflate(R.menu.recyclerview_class_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.actionDelete_menuRecyclerView:
                                DeleteClass(i);
                                break;
                            case R.id.actionMaterials_menuRecyclerView:
                                Intent goToMaterialsManager = new Intent(mContext, MaterialManager.class);
                                goToMaterialsManager.putExtra("class", mData.get(i).getId());
                                mContext.startActivity(goToMaterialsManager);
                                break;
                            case R.id.actionContent_menuRecyclerView:
                                Intent goToContentManager = new Intent(mContext, ContentManager.class);
                                goToContentManager.putExtra("class", mData.get(i).getId());
                                mContext.startActivity(goToContentManager);
                                break;
                            case R.id.actionEdit_menuRecyclerView:
                                Gson gson = new Gson();

                                Intent goToUpdateClass = new Intent(mContext, UpdateClass.class);
                                goToUpdateClass.putExtra("class", gson.toJson(mData.get(i)));
                                mContext.startActivity(goToUpdateClass);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(RecyclerViewAdapterClass.ClickListener clickListener) {
        RecyclerViewAdapterClass.clickListener = clickListener;
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView nClass, room, start, end, iconMenu, day;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);

            nClass = (TextView) itemView.findViewById(R.id.nClassItemClass);
            room = (TextView) itemView.findViewById(R.id.roomItemClass);
            start = (TextView) itemView.findViewById(R.id.startItemClass);
            end = (TextView) itemView.findViewById(R.id.endItemClass);
            iconMenu = (TextView) itemView.findViewById(R.id.options_irvClassDiscipline);
            day = (TextView) itemView.findViewById(R.id.dayClass);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(v,getAdapterPosition());
            return false;
        }
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemLongClick( View v, int position);
    }

    public void DeleteClass(int position) {

        int idClass = mData.get(position).getId();

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteClass(idClass);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() > 0) {
                        mData.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, R.string.success_deleteClass, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(mContext, R.string.error_deleteClass, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Erro eliminar", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
