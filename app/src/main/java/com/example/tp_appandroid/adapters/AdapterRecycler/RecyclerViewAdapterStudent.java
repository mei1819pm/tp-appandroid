package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.StudentModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapterStudent extends RecyclerView.Adapter<RecyclerViewAdapterStudent.ViewHolderStudent>{

    private Context mContext;
    private int idDiscipline;
    private List<StudentModel> mData;
    private static ClickListener clickListener;

    public RecyclerViewAdapterStudent(Context mContext, List<StudentModel> mData, int idDiscipline) {
        this.mContext = mContext;
        this.mData = mData;
        this.idDiscipline = idDiscipline;
    }

    @NonNull
    @Override
    public ViewHolderStudent onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_student_discipline, viewGroup, false);
        ViewHolderStudent viewHolderStudent = new ViewHolderStudent(v);

        return viewHolderStudent;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderStudent viewHolderCourse, int i) {

        viewHolderCourse.student.setText(mData.get(i).getName());
        viewHolderCourse.number.setText(mData.get(i).getNumber() + "");

        if(mData.get(i).getDelegate() != 1){
            viewHolderCourse.delegate.setText("Aluno");
        } else {
            viewHolderCourse.delegate.setText("Delegado");
        }

        viewHolderCourse.iconMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolderCourse.iconMenu);
                popupMenu.inflate(R.menu.recyclerview_menu_delete);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.actionDelete_menuRecyclerViewDelete:
                                DeleteStudent(i);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(RecyclerViewAdapterStudent.ClickListener clickListener) {
        RecyclerViewAdapterStudent.clickListener = clickListener;
    }

    public static class ViewHolderStudent extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView student, number, delegate, iconMenu;

        public ViewHolderStudent(@NonNull View itemView) {
            super(itemView);

            student = (TextView) itemView.findViewById(R.id.student_irvStudentDiscipline);
            number = (TextView) itemView.findViewById(R.id.number_irvStudentDiscipline);
            delegate = (TextView) itemView.findViewById(R.id.delegate_irvStudentDiscipline);
            iconMenu = (TextView) itemView.findViewById(R.id.options_irvStudentDiscipline);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(v,getAdapterPosition());
            return false;
        }
    }

    public void DeleteStudent(int position) {

        int idSutdent = mData.get(position).getId();

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteStudent(idDiscipline, idSutdent);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() > 0) {
                        mData.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, R.string.success_deleteStudent, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(mContext, R.string.error_deleteStudent, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemLongClick( View v, int position);
    }
}
