package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.models.ClassModel;

import java.util.List;

public class RecyclerViewAdapterTodayClass extends RecyclerView.Adapter<RecyclerViewAdapterTodayClass.ViewHolderTodayClass> {

    private Context mContext;
    private List<ClassModel> mData;

    public RecyclerViewAdapterTodayClass(Context mContext, List<ClassModel> mData) {
        this.mContext = mContext;
        this.mData= mData;
    }

    @NonNull
    @Override
    public ViewHolderTodayClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_today_class,viewGroup, false);
        ViewHolderTodayClass vHolder = new ViewHolderTodayClass(v);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTodayClass viewHolderTodayClass, int i) {
        viewHolderTodayClass.nClass.setText("Aula nº " + mData.get(i).getNumber() +"");
        viewHolderTodayClass.discipline.setText(mData.get(i).getDiscipline());
        viewHolderTodayClass.course.setText(mData.get(i).getCourse());
        viewHolderTodayClass.room.setText(mData.get(i).getClassRoom());
        viewHolderTodayClass.start.setText(mData.get(i).getStart());
        viewHolderTodayClass.end.setText(mData.get(i).getEnd());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolderTodayClass extends RecyclerView.ViewHolder {

        private TextView nClass, discipline, course, room, start, end;

        public ViewHolderTodayClass(@NonNull View itemView) {
            super(itemView);

            nClass = (TextView) itemView.findViewById(R.id.nClassItemTodayClass);
            discipline = (TextView) itemView.findViewById(R.id.disciplineItemTodayClass);
            course = (TextView) itemView.findViewById(R.id.courseItemTodayClass);
            room = (TextView) itemView.findViewById(R.id.roomItemClassToday);
            start = (TextView) itemView.findViewById(R.id.startItemClassToday);
            end = (TextView) itemView.findViewById(R.id.endItemTodayClass);

        }
    }
}
