package com.example.tp_appandroid.adapters.AdapterFragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.tp_appandroid.today.FragmentTodayClass;
import com.example.tp_appandroid.today.FragmentTodayContactHour;
import com.example.tp_appandroid.today.FragmentTodayJobBelivery;

public class FragmentPagerAdapterToday extends FragmentPagerAdapter {

    private  String[] tabTitleToday = new String[] {"Aulas", "Entregas", "Apoios"};
    Context context;
    private int pagecount = 3;

    public FragmentPagerAdapterToday(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        FragmentTodayClass fragmentTodayClass = new FragmentTodayClass();

        if(position == 0) {
            return fragmentTodayClass;

        } else if(position == 1) {
            FragmentTodayJobBelivery fragmentTodayJobBelivery = new FragmentTodayJobBelivery();
            return fragmentTodayJobBelivery;

        } else  if(position == 2) {
            FragmentTodayContactHour fragmentTodayContactHour = new FragmentTodayContactHour();
            return fragmentTodayContactHour;

        } else {
            return fragmentTodayClass;
        }
    }

    @Override
    public int getCount() {
        return pagecount;
        // return  0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // return super.getPageTitle(position);
        return  tabTitleToday[position];
    }
}
