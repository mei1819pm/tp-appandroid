package com.example.tp_appandroid.adapters.AdapterSpinner;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.tp_appandroid.models.SchoolYearModel;

import java.util.List;

public class SpinnerAdapterSchoolYear extends ArrayAdapter<SchoolYearModel> {
    private Context context;
    private List<SchoolYearModel> values;

    public SpinnerAdapterSchoolYear(Context context, int textViewResourceId, List<SchoolYearModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public SchoolYearModel getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText( values.get(position).getDescription());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);

        label.setText(values.get(position).getDescription());

        return label;
    }
}
