package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.course.UpdateCourse;
import com.example.tp_appandroid.models.CourseModel;
import com.google.gson.Gson;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapterCourse extends RecyclerView.Adapter<RecyclerViewAdapterCourse.ViewHolderCourse> {

    private Context mContext;
    private List<CourseModel> mData;
    private static ClickListener clickListener;

    public RecyclerViewAdapterCourse(Context mContext, List<CourseModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolderCourse onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_course, viewGroup, false);
        ViewHolderCourse viewHolderTodayJob = new ViewHolderCourse(v);

        return viewHolderTodayJob;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCourse viewHolderCourse, int i) {

        viewHolderCourse.course.setText(mData.get(i).getDescription());
        viewHolderCourse.semester.setText(mData.get(i).getSemester());
        viewHolderCourse.iconMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolderCourse.iconMenu);
                popupMenu.inflate(R.menu.recyclerview_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case  R.id.actionEdit_menuRecyclerView:
                                Gson gson = new Gson();

                                Intent goToUpdateCourse = new Intent(mContext, UpdateCourse.class);
                                goToUpdateCourse.putExtra("courseUpdate", gson.toJson(mData.get(i)));
                                mContext.startActivity(goToUpdateCourse);

                                break;

                            case R.id.actionDelete_menuRecyclerView:
                                DeleteCourse(i);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        RecyclerViewAdapterCourse.clickListener = clickListener;
    }

    public static class ViewHolderCourse extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView course, semester, iconMenu;

        public ViewHolderCourse(@NonNull View itemView) {
            super(itemView);

            course = (TextView) itemView.findViewById(R.id.course_irvCourse);
            semester = (TextView) itemView.findViewById(R.id.semester_irvCourse);
            iconMenu = (TextView) itemView.findViewById(R.id.options_irvCourse);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(v,getAdapterPosition());
            return false;
        }
    }

    public void DeleteCourse(int position) {

        int idCourse = mData.get(position).getId();

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteCourse(idCourse);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() > 0) {
                        mData.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, R.string.success_deleteCourse, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(mContext, R.string.error_deleteCurse, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemLongClick( View v, int position);
    }
}
