package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.models.SchoolYearModel;

import java.util.List;

public class RecyclerViewAdapterSchoolYears extends RecyclerView.Adapter<RecyclerViewAdapterSchoolYears.ViewHolderSchoolYears> {

    private Context mContext;
    private List<SchoolYearModel> mData;
    private static ClickListener clickListener;

    public RecyclerViewAdapterSchoolYears(Context mContext, List<SchoolYearModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolderSchoolYears onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_schoolyears, viewGroup, false);
        ViewHolderSchoolYears viewHolderTodayJob = new ViewHolderSchoolYears(v);

        return viewHolderTodayJob;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderSchoolYears viewHolderTodayJob, int i) {

        viewHolderTodayJob.schoolYears.setText(mData.get(i).getDescription());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        RecyclerViewAdapterSchoolYears.clickListener = clickListener;
    }

    public static class ViewHolderSchoolYears extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView schoolYears;

        public ViewHolderSchoolYears(@NonNull View itemView) {
            super(itemView);

            schoolYears = (TextView) itemView.findViewById(R.id.schoolYears_irvSchoolYears);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(v,getAdapterPosition());
            return false;
        }
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemLongClick( View v, int position);
    }

}
