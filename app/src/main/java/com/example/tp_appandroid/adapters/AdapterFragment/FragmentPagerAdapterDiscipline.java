package com.example.tp_appandroid.adapters.AdapterFragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.tp_appandroid.discipline.Fragments.FragmentAttendanceDiscipline;
import com.example.tp_appandroid.discipline.Fragments.FragmentClassDiscipline;
import com.example.tp_appandroid.discipline.Fragments.FragmentContactHourDiscipline;
import com.example.tp_appandroid.discipline.Fragments.FragmentJobDeliveryDiscipline;
import com.example.tp_appandroid.discipline.Fragments.FragmentStudentDiscipline;

public class FragmentPagerAdapterDiscipline extends FragmentPagerAdapter {

    private  String[] tabTitleToday = new String[] {"Alunos", "Aulas", "Entregas", "Apoios", "Horário Atendimento"};
    Context context;
    private int pagecount = 5;

    public FragmentPagerAdapterDiscipline(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        FragmentStudentDiscipline fragmentStudentDiscipline = new FragmentStudentDiscipline();

        if(position == 0) {
            return fragmentStudentDiscipline;

        } else if(position == 1) {
            FragmentClassDiscipline fragmentClassDiscipline = new FragmentClassDiscipline();
            return fragmentClassDiscipline;

        } else  if(position == 2) {
           FragmentJobDeliveryDiscipline fragmentJobDeliveryDiscipline = new FragmentJobDeliveryDiscipline();
           return fragmentJobDeliveryDiscipline;

        } else  if(position == 3) {
            FragmentContactHourDiscipline fragmentContactHourDiscipline = new FragmentContactHourDiscipline();
            return fragmentContactHourDiscipline;

        } else  if(position == 4) {
            FragmentAttendanceDiscipline fragmentAttendanceDiscipline = new FragmentAttendanceDiscipline();
            return fragmentAttendanceDiscipline;

        } else {
            return fragmentStudentDiscipline;
        }
    }

    @Override
    public int getCount() {
        return pagecount;
        // return  0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // return super.getPageTitle(position);
        return  tabTitleToday[position];
    }
}
