package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.discipline.UpdateDiscipline;
import com.example.tp_appandroid.models.DisciplineModel;
import com.google.gson.Gson;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewAdapterDiscipline extends RecyclerView.Adapter<RecyclerViewAdapterDiscipline.ViewHolderDiscipline> {

    private Context mContext;
    private List<DisciplineModel> mData;
    private static ClickListener clickListener;

    public RecyclerViewAdapterDiscipline(Context mContext, List<DisciplineModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolderDiscipline onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_discipline, viewGroup, false);
        ViewHolderDiscipline viewHolderTodayJob = new ViewHolderDiscipline(v);

        return viewHolderTodayJob;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDiscipline viewHolderDiscipline, int i) {

        viewHolderDiscipline.name.setText(mData.get(i).getName());
        viewHolderDiscipline.semester.setText(mData.get(i).getSemester());
        viewHolderDiscipline.iconMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolderDiscipline.iconMenu);
                popupMenu.inflate(R.menu.recyclerview_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case  R.id.actionEdit_menuRecyclerView:
                                Gson gson = new Gson();

                                Intent goToUpdateDiscipline = new Intent(mContext, UpdateDiscipline.class);
                                goToUpdateDiscipline.putExtra("disciplineUpdate", gson.toJson(mData.get(i)));
                                mContext.startActivity(goToUpdateDiscipline);
                                break;
                            case R.id.actionDelete_menuRecyclerView:
                                DeleteDiscipline(i);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        RecyclerViewAdapterDiscipline.clickListener = clickListener;
    }

    public static class ViewHolderDiscipline extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView name, semester, iconMenu;

        public ViewHolderDiscipline(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name_irvDiscipline);
            semester = (TextView) itemView.findViewById(R.id.semester_irvDiscipline);
            iconMenu = (TextView) itemView.findViewById(R.id.options_irvDiscipline);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(v,getAdapterPosition());
            return false;
        }
    }

    public void DeleteDiscipline(int position) {

        int idDiscipline = mData.get(position).getId();

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteDiscipline(idDiscipline);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() > 0) {
                        mData.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, R.string.success_deleteDiscipline, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(mContext, R.string.error_deleteDiscipline, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemLongClick( View v, int position);
    }
}
