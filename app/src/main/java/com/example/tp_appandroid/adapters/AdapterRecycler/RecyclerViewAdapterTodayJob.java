package com.example.tp_appandroid.adapters.AdapterRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.tp_appandroid.R;
import com.example.tp_appandroid.models.JobDeliveryModel;

import java.util.List;

public class RecyclerViewAdapterTodayJob extends RecyclerView.Adapter<RecyclerViewAdapterTodayJob.ViewHolderTodayJob> {

    private Context mContext;
    private List<JobDeliveryModel> mData;

    public RecyclerViewAdapterTodayJob(Context mContext, List<JobDeliveryModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolderTodayJob onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v;
        v= LayoutInflater.from(mContext).inflate(R.layout.item_recyclerview_today_job, viewGroup, false);
        ViewHolderTodayJob viewHolderTodayJob = new ViewHolderTodayJob(v);

        return viewHolderTodayJob;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTodayJob viewHolderTodayJob, int i) {

        viewHolderTodayJob.descriptionItemJob.setText(mData.get(i).getDescription());
        viewHolderTodayJob.disciplineItemJob.setText(mData.get(i).getDiscipline());
        viewHolderTodayJob.courseItemJob.setText(mData.get(i).getCourse());
        viewHolderTodayJob.locationItemJob.setText(mData.get(i).getDeliveryLocation());
        viewHolderTodayJob.hourLimitItemJob.setText(mData.get(i).getMaxTime());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolderTodayJob extends RecyclerView.ViewHolder {

        private TextView descriptionItemJob, disciplineItemJob, courseItemJob, locationItemJob, hourLimitItemJob;

        public ViewHolderTodayJob(@NonNull View itemView) {
            super(itemView);

            descriptionItemJob = (TextView) itemView.findViewById(R.id.descriptionItemTodayJob);
            disciplineItemJob = (TextView) itemView.findViewById(R.id.disciplineItemTodayJob);
            courseItemJob = (TextView) itemView.findViewById(R.id.courseItemTodayJob);
            locationItemJob = (TextView) itemView.findViewById(R.id.locationItemTodayJob);
            hourLimitItemJob = (TextView) itemView.findViewById(R.id.limitHourItemTodayJob);
        }
    }
}
