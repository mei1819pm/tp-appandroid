package com.example.tp_appandroid.planification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.dialogs.DialogAlert;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterCourse;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.course.AddCourse;
import com.example.tp_appandroid.export.ExportPlanification;
import com.example.tp_appandroid.models.CourseModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Course extends AppCompatActivity {

    private int idSchoolYear;
    private Context thisContext;

    private FloatingActionButton btnAdd;

    private RecyclerView recyclerViewCourse;
    private List<CourseModel> listCourse = new ArrayList<>();
    private RecyclerViewAdapterCourse adapterCourse;

    private DialogAlert alert = new DialogAlert();

    private SharedPreferences userSession;
    private int idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent getIntent = getIntent();
        idSchoolYear = getIntent.getIntExtra("idSchoolYear",1);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        btnAdd = (FloatingActionButton) findViewById(R.id.btnAdd_Course);

        ConstRecyclerView();

        apiGetCourse();

        btnAdd.setOnClickListener(v-> {
          Intent goToAddCourse = new Intent(thisContext, AddCourse.class);
          goToAddCourse.putExtra("idSchoolYear", idSchoolYear);
          startActivity(goToAddCourse);
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        apiGetCourse();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu_export, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.actionExport_menuExport:
                 alert.alertExport(thisContext, "Exportar Planificações", "Pretende exportar a planifcação de todos os cursos?", new DialogAlert.Callback() {
                     @Override
                     public void onSucess(int response) {
                         if(response == 1){
                             Intent goToExport = new Intent(thisContext, ExportPlanification.class);
                             goToExport.putExtra("idSchoolYear", idSchoolYear);
                             startActivity(goToExport);
                         }
                     }
                 });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void apiGetCourse() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<CourseModel>> call = service.getCourseByUserSchoolYear(idUser, idSchoolYear);
        call.enqueue(new Callback<List<CourseModel>>() {
            @Override
            public void onResponse(Call<List<CourseModel>> call, Response<List<CourseModel>> response) {
                if (response.code() == 200) {


                    if (response.body().size() > 0) {
                        listCourse = response.body();
                        ConstRecyclerView();
                        findViewById(R.id.courseEmpty_Course).setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CourseModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ConstRecyclerView() {

        recyclerViewCourse = (RecyclerView) findViewById(R.id.recyclerview_course);
        adapterCourse = new RecyclerViewAdapterCourse(thisContext, listCourse);
        recyclerViewCourse.setLayoutManager(new LinearLayoutManager(thisContext));
        recyclerViewCourse.setAdapter(adapterCourse);

        adapterCourse.setOnItemClickListener(new RecyclerViewAdapterCourse.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent goToDiscipline = new Intent(thisContext, Disciplines.class);
                goToDiscipline.putExtra("idSchoolYear",idSchoolYear );
                goToDiscipline.putExtra("idCourse", listCourse.get(position).getId());
                startActivity(goToDiscipline);
            }

            @Override
            public void onItemLongClick(View v, int position) {

            }

        });
    }

}
