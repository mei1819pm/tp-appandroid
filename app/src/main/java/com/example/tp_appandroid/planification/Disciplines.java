package com.example.tp_appandroid.planification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.dialogs.DialogAlert;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterDiscipline;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.discipline.AddDiscipline;
import com.example.tp_appandroid.models.DisciplineModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Disciplines extends AppCompatActivity {

    private Context thisContext;
    private int idSchoolYear;
    private int idCourse;

    private FloatingActionButton btnAdd;

    private RecyclerView recyclerViewDiscipline;
    private List<DisciplineModel> listDiscipline = new ArrayList<>();
    private RecyclerViewAdapterDiscipline adapterDiscipline;

    private DialogAlert alert = new DialogAlert();

    private SharedPreferences userSession;
    private int idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disciplines);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent getIntent = getIntent();
        idSchoolYear = getIntent.getIntExtra("idSchoolYear", 1);
        idCourse = getIntent.getIntExtra("idCourse", 1);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        btnAdd = (FloatingActionButton) findViewById(R.id.btnAdd_Discipline);

        ConstRecyclerView();

        apiGetDiscipline();

        btnAdd.setOnClickListener(v -> {
            Intent goToAddDiscipline = new Intent(thisContext, AddDiscipline.class);
            goToAddDiscipline.putExtra("idCourse", idCourse);
            goToAddDiscipline.putExtra("idSY", idSchoolYear);
            startActivity(goToAddDiscipline);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        apiGetDiscipline();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void apiGetDiscipline() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<DisciplineModel>> call = service.getDisciplineByCourse(idUser, idCourse);
        call.enqueue(new Callback<List<DisciplineModel>>() {

            @Override
            public void onResponse(Call<List<DisciplineModel>> call, Response<List<DisciplineModel>> response) {
                if (response.code() == 200 && response.body().size() > 0) {
                    listDiscipline = response.body();
                    ConstRecyclerView();
                    //findViewById(R.id.courseEmpty_Course).setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<DisciplineModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ConstRecyclerView() {
        recyclerViewDiscipline = (RecyclerView) findViewById(R.id.recyclerview_discipline);
        adapterDiscipline = new RecyclerViewAdapterDiscipline(thisContext, listDiscipline);
        recyclerViewDiscipline.setLayoutManager(new LinearLayoutManager(thisContext));
        recyclerViewDiscipline.setAdapter(adapterDiscipline);

        adapterDiscipline.setOnItemClickListener(new RecyclerViewAdapterDiscipline.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent discipline = new Intent(thisContext, Discipline.class);
                discipline.putExtra("name", listDiscipline.get(position).getName());
                discipline.putExtra("id", listDiscipline.get(position).getId());
                startActivity(discipline);
            }

            @Override
            public void onItemLongClick(View v, int position) {

            }
        });
    }
}
