package com.example.tp_appandroid.planification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.dialogs.DialogAlert;
import com.example.tp_appandroid.discipline.CopyDiscipline.CopyDiscipline;
import com.example.tp_appandroid.adapters.AdapterFragment.FragmentPagerAdapterDiscipline;
import com.example.tp_appandroid.email.PrepareEmail;
import com.example.tp_appandroid.export.ExportPlanification;

public class Discipline extends AppCompatActivity {

    private Context thisContext;
    private int idDiscipline;
    private String nameDiscipline;

    private SharedPreferences userSession;
    private int idUser;

    private ViewPager viewPager;
    private TabLayout tabLayout;

    private DialogAlert alert = new DialogAlert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discipline);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent getIntent = getIntent();
        idDiscipline = getIntent.getIntExtra("id", 1);
        nameDiscipline = getIntent.getStringExtra("name");

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        viewPager = (ViewPager) findViewById(R.id.viewPagerDiscipline);
        viewPager.setAdapter(new FragmentPagerAdapterDiscipline(getSupportFragmentManager(), this));

        tabLayout = (TabLayout) findViewById(R.id.tabDiscipline);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        setTitle(nameDiscipline);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu_discipline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.actionExport_menuDiscipline:
                alert.alertExport(thisContext, "Exportar Planificações", "Pretende exportar a planifcação de todos os cursos?", new DialogAlert.Callback() {
                    @Override
                    public void onSucess(int response) {
                        if(response == 1){
                            Intent goToExport = new Intent(thisContext, ExportPlanification.class);
                            goToExport.putExtra("idDiscipline", idDiscipline);
                            startActivity(goToExport);
                        }
                    }
                });
                return true;
            case R.id.actionSendEmail_menuDiscipline:
                Intent aux = new Intent(thisContext, PrepareEmail.class);
                aux.putExtra("discipline", idDiscipline);
                startActivity(aux);
                return true;

            case R.id.actionCopyDiscipline_menuDiscipline:
                Intent goToCopy = new Intent(thisContext, CopyDiscipline.class);
                goToCopy.putExtra("discipline", idDiscipline);
                startActivity(goToCopy);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public int GetIdDiscipline() {
        return idDiscipline;
    }
}
