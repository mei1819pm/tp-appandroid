package com.example.tp_appandroid.planification;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.example.tp_appandroid.MainActivity;
import com.example.tp_appandroid.R;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterSchoolYears;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.calendar.Calendar;
import com.example.tp_appandroid.models.SchoolYearModel;
import com.example.tp_appandroid.settings.Settings;
import com.example.tp_appandroid.today.HomeApp;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolYears extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Context thisContext;
    private SharedPreferences userSession;
    private DrawerLayout drawer;

    private RecyclerView recyclerViewSchoolYears;
    private List<SchoolYearModel> listSchoolYears = new ArrayList<>();
    RecyclerViewAdapterSchoolYears adapterSchoolYears;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_years);


        thisContext = SchoolYears.this;

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        ConstRecyclerView();

        apiGetSchoolYears();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_today:
                Intent intentHome = new Intent(thisContext, HomeApp.class);
                startActivity(intentHome);
                break;

            case R.id.nav_calendar:
                Intent intentCalendar = new Intent(thisContext, Calendar.class);
                startActivity(intentCalendar);
                break;

            case R.id.nav_settings:
                Intent intentSettings = new Intent(thisContext, Settings.class);
                startActivity(intentSettings);
                break;

            case R.id.nav_logout:
                SharedPreferences.Editor editor = userSession.edit();
                editor.clear();
                editor.commit();

                Intent intentGoLogin = new Intent(thisContext, MainActivity.class);
                this.finish();
                startActivity(intentGoLogin);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void apiGetSchoolYears() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<SchoolYearModel>> call = service.getAllScoolyears();
        call.enqueue(new Callback<List<SchoolYearModel>>() {
            @Override
            public void onResponse(Call<List<SchoolYearModel>> call, Response<List<SchoolYearModel>> response) {
                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        listSchoolYears = response.body();
                        ConstRecyclerView();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<SchoolYearModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ConstRecyclerView() {

        recyclerViewSchoolYears = (RecyclerView) findViewById(R.id.recyclerview_SchoolYears);
        adapterSchoolYears = new RecyclerViewAdapterSchoolYears(thisContext, listSchoolYears);
        recyclerViewSchoolYears.setLayoutManager(new LinearLayoutManager(thisContext));
        recyclerViewSchoolYears.setAdapter(adapterSchoolYears);

        adapterSchoolYears.setOnItemClickListener(new RecyclerViewAdapterSchoolYears.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent goToCourse = new Intent(thisContext, Course.class);
                goToCourse.putExtra("idSchoolYear",listSchoolYears.get(position).getId() );
                startActivity(goToCourse);
            }

            @Override
            public void onItemLongClick(View v, int position) {

            }

        });
    }
}
