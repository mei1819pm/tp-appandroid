package com.example.tp_appandroid.today;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.tp_appandroid.calendar.Calendar;
import com.example.tp_appandroid.MainActivity;
import com.example.tp_appandroid.R;
import com.example.tp_appandroid.planification.SchoolYears;
import com.example.tp_appandroid.settings.Settings;
import com.example.tp_appandroid.adapters.AdapterFragment.FragmentPagerAdapterToday;
import com.example.tp_appandroid.service_notification.NotificationService;

public class HomeApp extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private SharedPreferences userSession;

    private TabLayout tabTodayOptions;
    private ViewPager viewPagerToday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_app);


        userSession = getSharedPreferences("userSession_app",MODE_PRIVATE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Colocar o azul a dizer que esta situado nesta pagina automaticamente por causa de serem atividades
        navigationView.setCheckedItem(R.id.nav_today);

        //Resto da Pagina
        viewPagerToday = (ViewPager) findViewById(R.id.viewPagerToday);
        viewPagerToday.setAdapter(new FragmentPagerAdapterToday(getSupportFragmentManager(), this));
        //Cabeçalho
        tabTodayOptions = (TabLayout) findViewById(R.id.tabTodayOptions);
        tabTodayOptions.setupWithViewPager(viewPagerToday);
        tabTodayOptions.setTabGravity(TabLayout.GRAVITY_FILL);

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_settings:
                Intent intentSettings = new Intent(HomeApp.this, Settings.class);
                startActivity(intentSettings);
                break;

            case R.id.nav_calendar:
                Intent intentCalendar = new Intent(HomeApp.this, Calendar.class);
                startActivity(intentCalendar);
                break;

            case R.id.nav_planification:
                Intent intentPlanification = new Intent(HomeApp.this, SchoolYears.class);
                startActivity(intentPlanification);

                break;

            case R.id.nav_logout:
                SharedPreferences.Editor editor = userSession.edit();
                editor.clear();
                editor.commit();

                Intent intentGoLogin = new Intent(HomeApp.this, MainActivity.class);
                this.finish();
                Intent stopServiceIntent = new Intent(HomeApp.this, NotificationService.class);
                stopService(stopServiceIntent);
                startActivity(intentGoLogin);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

}
