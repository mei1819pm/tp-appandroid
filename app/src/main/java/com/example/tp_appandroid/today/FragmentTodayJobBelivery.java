package com.example.tp_appandroid.today;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.tp_appandroid.other_class.NoConnection;
import com.example.tp_appandroid.R;
import com.example.tp_appandroid.other_class.CheckConnection;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterTodayJob;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.JobDeliveryModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class FragmentTodayJobBelivery extends Fragment {

    private View v;
    private RecyclerView recyclerView;
    private List<JobDeliveryModel> listJobDelivery;

    private CheckConnection checkConnection;
    private SharedPreferences userSession;

    private int showMassage = 1;

    public FragmentTodayJobBelivery() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_today_jobbelivery, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview_today_job);
        RecyclerViewAdapterTodayJob recyclerViewAdapterTodayJob = new RecyclerViewAdapterTodayJob(getContext(), listJobDelivery);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerViewAdapterTodayJob);


        if(showMassage == 0) {
            GoneMessage();
        }

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userSession = this.getActivity().getSharedPreferences("userSession_app", MODE_PRIVATE);
        int idUser = userSession.getInt("idUser", 0);

        listJobDelivery = new ArrayList<>();

        // Verificar Ligação
        checkConnection = new CheckConnection(getContext());
        if (!checkConnection.isConnected()) {
            Intent intentNoConnection = new Intent(getContext(), NoConnection.class);
            startActivity(intentNoConnection);
        }

        apiGetJobToday(idUser);

    }

    private void apiGetJobToday(int auxIdUser) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<JobDeliveryModel>> call = service.getJobBeliveryToday(auxIdUser);
        call.enqueue(new Callback<List<JobDeliveryModel>>() {
            @Override
            public void onResponse(Call<List<JobDeliveryModel>> call, Response<List<JobDeliveryModel>> response) {
                if (response.code() == 200) {


                    if (response.body().size() > 0) {
                        listJobDelivery = response.body();
                        RecyclerViewAdapterTodayJob recyclerViewAdapterTodayJob = new RecyclerViewAdapterTodayJob(getContext(), listJobDelivery);
                        recyclerView.setAdapter(recyclerViewAdapterTodayJob);
                        showMassage = 0;
                        GoneMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<JobDeliveryModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void GoneMessage() {
        TextView empty = v.findViewById(R.id.jobDeliveryEmpty_today);
        empty.setVisibility(View.GONE);
    }

}
