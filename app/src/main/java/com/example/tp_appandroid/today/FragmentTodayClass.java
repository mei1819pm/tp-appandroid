package com.example.tp_appandroid.today;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.other_class.NoConnection;
import com.example.tp_appandroid.other_class.CheckConnection;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterTodayClass;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.R;

import java.util.ArrayList;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class FragmentTodayClass extends Fragment {

    private CheckConnection checkConnection;
    private SharedPreferences userSession;

    private View v;
    private RecyclerView recyclerViewTodayClass;
    private List<ClassModel> classes;

    private int showMassage = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_today_class, container, false);
        recyclerViewTodayClass = (RecyclerView) v.findViewById(R.id.recyclerview_today_class);
        RecyclerViewAdapterTodayClass recyclerViewAdapterTodayClass = new RecyclerViewAdapterTodayClass(getContext(), classes);
        recyclerViewTodayClass.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewTodayClass.setAdapter(recyclerViewAdapterTodayClass);

        if(showMassage == 0) {
            GoneMessage();
        }

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        userSession = this.getActivity().getSharedPreferences("userSession_app", MODE_PRIVATE);
        int idUser = userSession.getInt("idUser", 0);

        classes = new ArrayList<>();

        // Verificar Ligação
        checkConnection = new CheckConnection(getContext());
        if (!checkConnection.isConnected()) {
            Intent intentNoConnection = new Intent(getContext(), NoConnection.class);
            startActivity(intentNoConnection);
        }

        apiGetClassToday(idUser);

    }

    private void apiGetClassToday(int auxIdUser) {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<ClassModel>> cal = service.getClassToday(auxIdUser);
        cal.enqueue(new Callback<List<ClassModel>>() {
            @Override
            public void onResponse(Call<List<ClassModel>> call, Response<List<ClassModel>> response) {

                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        classes = response.body();
                        //Carregar o fragment com os dados
                        RecyclerViewAdapterTodayClass recyclerViewAdapterTodayClass = new RecyclerViewAdapterTodayClass(getContext(), classes);
                        recyclerViewTodayClass.setAdapter(recyclerViewAdapterTodayClass);
                        showMassage = 0;
                        GoneMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ClassModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void GoneMessage() {
        TextView empty = v.findViewById(R.id.classEmpty_today);
        empty.setVisibility(View.GONE);
    }

}
