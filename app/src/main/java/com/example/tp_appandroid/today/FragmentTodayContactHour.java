package com.example.tp_appandroid.today;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.other_class.NoConnection;
import com.example.tp_appandroid.R;
import com.example.tp_appandroid.other_class.CheckConnection;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterTodayContact;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.ContactHourModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class FragmentTodayContactHour extends Fragment {

    private CheckConnection checkConnection;
    private SharedPreferences userSession;

    View v;

    private RecyclerView recyclerView;
    private List<ContactHourModel> listContactHour;

    private int showMassage = 1;

    public FragmentTodayContactHour() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_today_contacthour, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview_today_contact);
        RecyclerViewAdapterTodayContact recyclerViewAdapterTodayContact = new RecyclerViewAdapterTodayContact(getContext(), listContactHour);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerViewAdapterTodayContact);

        if(showMassage == 0) {
            GoneMessage();
        }

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userSession = this.getActivity().getSharedPreferences("userSession_app", MODE_PRIVATE);
        int idUser = userSession.getInt("idUser", 0);

        listContactHour = new ArrayList<>();

        // Verificar Ligação
        checkConnection = new CheckConnection(getContext());
        if (!checkConnection.isConnected()) {
            Intent intentNoConnection = new Intent(getContext(), NoConnection.class);
            startActivity(intentNoConnection);
        }

        apiGetContactHourToday(idUser);

    }

    private void apiGetContactHourToday(int auxIdUser) {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<ContactHourModel>> cal = service.getContactToday(auxIdUser);
        cal.enqueue(new Callback<List<ContactHourModel>>() {
            @Override
            public void onResponse(Call<List<ContactHourModel>> call, Response<List<ContactHourModel>> response) {
                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        listContactHour = response.body();
                        RecyclerViewAdapterTodayContact recyclerViewAdapterTodayContact = new RecyclerViewAdapterTodayContact(getContext(), listContactHour);
                        recyclerView.setAdapter(recyclerViewAdapterTodayContact);

                        showMassage = 0;
                        GoneMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ContactHourModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void GoneMessage() {
        TextView empty = v.findViewById(R.id.contactHourEmpty_today);
        empty.setVisibility(View.GONE);
    }
}
