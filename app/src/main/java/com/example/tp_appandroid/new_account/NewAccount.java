package com.example.tp_appandroid.new_account;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.MainActivity;
import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.TeacherModel;
import com.example.tp_appandroid.validate.ValidateInput;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewAccount extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinnerDegree;
    private EditText name;
    private EditText email;
    private EditText passwordOne;
    private EditText passwordTwo;
    private EditText phoneNumber;
    private EditText workPlace;
    private EditText graduated;
    private String degree;
    private int optionSpinner;

    private Button btnCreateAccount;

    private TeacherModel teacherRequest;
    private String sPasswordTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = (EditText) findViewById(R.id.name_newAccount);
        email = (EditText) findViewById(R.id.email_newAccount);
        passwordOne = (EditText) findViewById(R.id.passwordOne_newAccount);
        passwordTwo = (EditText) findViewById(R.id.passwordTwo_newAccount);
        phoneNumber = (EditText) findViewById(R.id.number_newAccount);
        workPlace = (EditText) findViewById(R.id.workPlace_newAccount);
        graduated = (EditText) findViewById(R.id.graduted_newAccount);

        btnCreateAccount = (Button) findViewById(R.id.btnCreate_newAccount);

        spinnerDegree = findViewById(R.id.spinnerDegree_newAccount);
        ArrayAdapter<CharSequence> adapterSpinner = ArrayAdapter.createFromResource(this, R.array.spiner_degree, android.R.layout.simple_spinner_item);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDegree.setAdapter(adapterSpinner);
        spinnerDegree.setOnItemSelectedListener(NewAccount.this);

        btnCreateAccount.setOnClickListener(v -> {

            teacherRequest = new TeacherModel();

            teacherRequest.setName(name.getText().toString());
            teacherRequest.setEmail(email.getText().toString());
            teacherRequest.setPassword(passwordOne.getText().toString());
            sPasswordTwo = passwordTwo.getText().toString();

            teacherRequest.setNumber(phoneNumber.getText().toString());
            teacherRequest.setUniversity(workPlace.getText().toString());
            teacherRequest.setGraduated(graduated.getText().toString());
            teacherRequest.setDescriptionDegree(degree);

            ValidateInput validateName =  new ValidateInput(teacherRequest.getName(), findViewById(R.id.boxName_newAccount));
            ValidateInput validateEmail =  new ValidateInput(teacherRequest.getEmail(), findViewById(R.id.boxEmail_newAccount));
            ValidateInput validatePassword =  new ValidateInput(teacherRequest.getPassword(), findViewById(R.id.boxPasswordOne_newAccount));
            ValidateInput validatePasswordTwo =  new ValidateInput(sPasswordTwo, findViewById(R.id.boxPasswordTwo_newAccount));
            ValidateInput validateNumber =  new ValidateInput(teacherRequest.getNumber(), findViewById(R.id.boxNumber_newAccount));
            ValidateInput validateUniversity =  new ValidateInput(teacherRequest.getUniversity(), findViewById(R.id.boxWorkPlace_newAccount));
            ValidateInput validateGraduated =  new ValidateInput(teacherRequest.getGraduated(), findViewById(R.id.boxGraduted_newAccount));

            if (!validateName.ValidateString() | !validateEmail.ValidateString() | !validatePassword.ValidatePassword(sPasswordTwo) | !validatePasswordTwo.ValidatePassword(teacherRequest.getPassword()) | !validateNumber.ValidateString() | !validateUniversity.ValidateString() | !validateGraduated.ValidateString()) {
                return;

            } else if (optionSpinner == 0) {
                Toast.makeText(NewAccount.this, R.string.error_degree, Toast.LENGTH_SHORT).show();

            } else {
                apiCreateAccount();

            }

        });
    }

    //Voltar Para Tras
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        degree = parent.getSelectedItem().toString();
        optionSpinner = position;

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void apiCreateAccount() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.createAccount(teacherRequest);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res == 1) {
                        Toast.makeText(NewAccount.this, R.string.success_createAccount, Toast.LENGTH_SHORT).show();
                        Intent intentGoLogin = new Intent(NewAccount.this, MainActivity.class);
                        NewAccount.this.finish();
                        startActivity(intentGoLogin);

                    } else if (res == 2) {
                        emailExist();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void emailExist() {
        ConstraintLayout layoutNewAccount = (ConstraintLayout) findViewById(R.id.layoutNewAccout);
        ConstraintSet set = new ConstraintSet();

        TextView textViewEmailExist = new TextView(NewAccount.this);
        textViewEmailExist.setId(View.generateViewId());
        textViewEmailExist.setText(R.string.error_emailExist);
        textViewEmailExist.setTextColor(NewAccount.this.getResources().getColor(R.color.colorRedError));
        layoutNewAccount.addView(textViewEmailExist, 0);
        set.clone(layoutNewAccount);

        set.connect(textViewEmailExist.getId(), ConstraintSet.RIGHT,
                findViewById(R.id.boxEmail_newAccount).getId(), ConstraintSet.RIGHT);

        set.connect(textViewEmailExist.getId(), ConstraintSet.TOP,
                findViewById(R.id.boxEmail_newAccount).getId(), ConstraintSet.TOP);

        set.applyTo(layoutNewAccount);
    }
}
