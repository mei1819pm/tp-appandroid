package com.example.tp_appandroid.validate;

import android.support.design.widget.TextInputLayout;

import com.example.tp_appandroid.R;

public class ValidateInput {

    private String text;
    private TextInputLayout boxInpout;

    public ValidateInput(String text, TextInputLayout boxInpout) {
        this.text = text;
        this.boxInpout = boxInpout;
    }

    public boolean ValidateString() {
         if (text.equals("")) {
             boxInpout.setErrorEnabled(true);
             boxInpout.setError("Tem que preencher o campo");
            return false;

        } else {
             boxInpout.setError(null);
             boxInpout.setErrorEnabled(false);
            return true;
        }
    }

    public boolean ValidatePassword(String passwordTwo) {
        if (text.equals("")) {
            boxInpout.setErrorEnabled(true);
            boxInpout.setError("Tem que preencher o campo");
            return false;

        } else if (!text.equals(passwordTwo)) {
            boxInpout.setErrorEnabled(true);
            boxInpout.setError("Palavras-Passe diferentes");
            return false;

        } else {
            boxInpout.setError(null);
            boxInpout.setErrorEnabled(false);
            return true;
        }
    }
}
