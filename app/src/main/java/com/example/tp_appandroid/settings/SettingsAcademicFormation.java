package com.example.tp_appandroid.settings;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.TeacherModel;
import com.example.tp_appandroid.validate.ValidateInput;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsAcademicFormation extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private int idUser;

    private EditText graduated;
    private String degree;
    private int startPositionSpinner;
    private Button btnUpdate;

    private Spinner spinnerDegree;
    private int optionSpinner;
    private int controlerSpinner = 0;

    private Context thisContext;

    private TeacherModel teacherRequest = new TeacherModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_academic_formation);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = SettingsAcademicFormation.this;

        graduated = (EditText) findViewById(R.id.graduted_settingsAcademicFormation);

        spinnerDegree = findViewById(R.id.spinnerDegree_settingsAcademicFormation);
        ArrayAdapter<CharSequence> adapterSpinner = ArrayAdapter.createFromResource(thisContext, R.array.spiner_degree, android.R.layout.simple_spinner_item);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDegree.setAdapter(adapterSpinner);
        spinnerDegree.setOnItemSelectedListener(SettingsAcademicFormation.this);

        btnUpdate = (Button) findViewById(R.id.btnUpdate_settingsAcademicFormation);

        Intent receivedSettings = getIntent();
        idUser = receivedSettings.getIntExtra("idUser", 0);
        graduated.setText(receivedSettings.getStringExtra("graduated"));
        degree = receivedSettings.getStringExtra("degree");

        getStartPositionSpinner();

        btnUpdate.setOnClickListener(v -> {

            teacherRequest.setGraduated(graduated.getText().toString());
            teacherRequest.setDescriptionDegree(degree);

            ValidateInput validateGraduated =  new ValidateInput(teacherRequest.getGraduated(), findViewById(R.id.boxGraduted_settingsAcademicFormation));

            if (!validateGraduated.ValidateString()) {
                return;
            } else if (optionSpinner == 0) {
                Toast.makeText(thisContext, R.string.error_degree, Toast.LENGTH_SHORT).show();

            } else {
                apiUpdataAcademicFormation(thisContext, idUser);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (controlerSpinner == 0) {
            parent.setSelection(startPositionSpinner);
            controlerSpinner = 1;
        }

        degree = parent.getSelectedItem().toString();
        optionSpinner = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void getStartPositionSpinner() {
        List<String> arraySpinner = Arrays.asList(getResources().getStringArray(R.array.spiner_degree));

        for (String aux : arraySpinner) {
            if (aux.equals(degree)) {
                startPositionSpinner = arraySpinner.indexOf(aux);
                break;
            }
        }
    }

    private void apiUpdataAcademicFormation(Context context, int auxIdUser) {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateAccedemicFormation(auxIdUser, teacherRequest);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        Intent goToSettings = new Intent(context, Settings.class);
                        SettingsAcademicFormation.this.finish();
                        startActivity(goToSettings);
                        Toast.makeText(context, R.string.success_updataData, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, R.string.error_updataData, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
