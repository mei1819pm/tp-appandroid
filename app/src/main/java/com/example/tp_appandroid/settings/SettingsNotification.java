package com.example.tp_appandroid.settings;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.export.ExportPlanification;
import com.example.tp_appandroid.models.TeacherModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsNotification extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private int idUser;
    private int notification;

    private Switch switchNotification;

    private Button btnUpdate;

    private Context thisContext;

    private TeacherModel teacherRequest = new TeacherModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_notification);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = SettingsNotification.this;

        switchNotification = (Switch) findViewById(R.id.switch_settingsNotification);
        btnUpdate = (Button) findViewById(R.id.btnUpdate_settingsNotification);

        Intent receivedSettings = getIntent();
        idUser = receivedSettings.getIntExtra("idUser",0);
        notification = receivedSettings.getIntExtra("notification", 0);

        switchNotification.setOnCheckedChangeListener(this);
        if(notification == 1) {
            switchNotification.setChecked(true);
        } else {
            switchNotification.setChecked(false);
        }

        btnUpdate.setOnClickListener(v-> {
            apiUpdataNotification(thisContext, idUser);
        });

    }

    //Voltar Para Tras
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked == true) {
           teacherRequest.setNotification(1);
        } else {
            teacherRequest.setNotification(0);
        }
    }

    public void apiUpdataNotification(Context context, int auxIdUser) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateNotification(auxIdUser, teacherRequest);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        Intent goToSettings = new Intent(context, Settings.class);
                        SettingsNotification.this.finish();
                        startActivity(goToSettings);
                        Toast.makeText(context, R.string.success_updataNotification, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, R.string.error_updateNotification, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
