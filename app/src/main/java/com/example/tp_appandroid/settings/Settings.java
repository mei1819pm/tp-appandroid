package com.example.tp_appandroid.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.example.tp_appandroid.dialogs.DialogAlert;
import com.example.tp_appandroid.calendar.Calendar;
import com.example.tp_appandroid.planification.SchoolYears;
import com.example.tp_appandroid.today.HomeApp;
import com.example.tp_appandroid.MainActivity;
import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.TeacherModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Settings extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private SharedPreferences userSession;

    private int idUser;

    private Context thisContext;

    private TeacherModel teacherResponse = new TeacherModel();

    private Button btnDeteleUser;
    private ConstraintLayout personalData, personalProfessional, academicFormation, notification;

    private DialogAlert alert = new DialogAlert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        thisContext = Settings.this;

        btnDeteleUser = (Button) findViewById(R.id.btnDeleteUserSettings);
        personalData = (ConstraintLayout) findViewById(R.id.personalData_settings);
        personalProfessional = (ConstraintLayout) findViewById(R.id.personalProfessional_settings);
        academicFormation = (ConstraintLayout) findViewById(R.id.academicFormation_settings);
        notification = (ConstraintLayout) findViewById(R.id.notification_settings);

        userSession = getSharedPreferences("userSession_app",MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Colocar o azul a dizer que esta situado nesta pagina automaticamente por causa de serem atividades
        navigationView.setCheckedItem(R.id.nav_settings);

        apiuserById(idUser);

        personalData.setOnClickListener(v-> {
            Intent gotToSttingsPersonalData = new Intent(thisContext, SettingsPersonalData.class);
            gotToSttingsPersonalData.putExtra("idUser", idUser);
            gotToSttingsPersonalData.putExtra("nameUser", teacherResponse.getName());
            gotToSttingsPersonalData.putExtra("email", teacherResponse.getEmail());
            gotToSttingsPersonalData.putExtra("password", teacherResponse.getPassword());
            gotToSttingsPersonalData.putExtra("number", teacherResponse.getNumber());
            startActivity(gotToSttingsPersonalData);

        });

        personalProfessional.setOnClickListener(v-> {
            Intent gotToSettingsProfessionalData = new Intent(thisContext, SettingsProfessionalData.class);
            gotToSettingsProfessionalData.putExtra("idUser", idUser);
            gotToSettingsProfessionalData.putExtra("workPlace", teacherResponse.getUniversity());
            startActivity(gotToSettingsProfessionalData);

        });

        academicFormation.setOnClickListener(v-> {
            Intent goToSettingsAcademicFormation = new Intent(thisContext, SettingsAcademicFormation.class);
            goToSettingsAcademicFormation.putExtra("idUser", idUser);
            goToSettingsAcademicFormation.putExtra("graduated", teacherResponse.getGraduated());
            goToSettingsAcademicFormation.putExtra("degree", teacherResponse.getDescriptionDegree());
            startActivity(goToSettingsAcademicFormation);

        });

        notification.setOnClickListener(v-> {
            Intent goToSettingsNotification = new Intent(thisContext, SettingsNotification.class);
            goToSettingsNotification.putExtra("idUser", idUser);
            goToSettingsNotification.putExtra("notification",teacherResponse.getNotification());
            startActivity(goToSettingsNotification);
        });

        btnDeteleUser.setOnClickListener(v-> {
            alert.alertExport(thisContext, "Eliminar Utilizador", "Pretende Eliminar o Utilizador?", new DialogAlert.Callback() {
                @Override
                public void onSucess(int response) {
                    if(response == 1){
                        apiDeleteUser(thisContext, idUser);
                    }
                }
            });
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_today:
                Intent intentHome = new Intent(thisContext, HomeApp.class);
                startActivity(intentHome);
                break;

            case R.id.nav_calendar:
                Intent intentCalendar = new Intent(thisContext, Calendar.class);
                startActivity(intentCalendar);
                break;

            case R.id.nav_planification:
                Intent intentPlanification = new Intent(thisContext, SchoolYears.class);
                startActivity(intentPlanification);
                break;

            case R.id.nav_logout:
                SharedPreferences.Editor editor = userSession.edit();
                editor.clear();
                editor.commit();

                Intent intentGoLogin = new Intent(thisContext, MainActivity.class);
                this.finish();
                startActivity(intentGoLogin);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    private void apiuserById(int auxIdUser) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<TeacherModel> call = service.getUserById(auxIdUser);
        call.enqueue(new Callback<TeacherModel>() {
            @Override
            public void onResponse(Call<TeacherModel> call, Response<TeacherModel> response) {
                if(response.code() == 200) {
                    teacherResponse = response.body();
                }
            }

            @Override
            public void onFailure(Call<TeacherModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void apiDeleteUser(Context context, int auxIdUser) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteUser(auxIdUser);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if(response.code() == 200) {

                    if(response.body() == 1) {

                        Intent intentGoLogin = new Intent(Settings.this, MainActivity.class);
                        SharedPreferences.Editor editor = userSession.edit();
                        editor.clear();
                        editor.commit();
                        Settings.this.finish();
                        startActivity(intentGoLogin);
                        Toast.makeText(context, R.string.success_deleteUser, Toast.LENGTH_SHORT).show();

                    }  else {
                        Toast.makeText(context, R.string.error_deleteUser, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
