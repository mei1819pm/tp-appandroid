package com.example.tp_appandroid.settings;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.TeacherModel;
import com.example.tp_appandroid.validate.ValidateInput;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsPersonalData extends AppCompatActivity {

    private EditText name;
    private EditText email;
    private EditText passwordOne;
    private EditText passwordTwo;
    private EditText phoneNumber;
    private String sPasswordTwo;

    private int idUser;

    private Button btnUpdate;

    private Context thisContext;

    private TeacherModel teacherRequest = new TeacherModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_personal_data);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = SettingsPersonalData.this;

        name = (EditText) findViewById(R.id.name_settingsPersonalData);
        email = (EditText) findViewById(R.id.email_settingsPersonalData);
        passwordOne = (EditText) findViewById(R.id.passwordOne_settingsPersonalData);
        passwordTwo = (EditText) findViewById(R.id.passwordTwo_settingsPersonalData);
        phoneNumber = (EditText) findViewById(R.id.number_settingsPersonalData);
        btnUpdate = (Button) findViewById(R.id.btnUpdate_settingsPersonalData);

        Intent receivedSettings = getIntent();
        idUser = receivedSettings.getIntExtra("idUser",0);
        name.setText(receivedSettings.getStringExtra("nameUser"));
        email.setText(receivedSettings.getStringExtra("email"));
        passwordOne.setText(receivedSettings.getStringExtra("password"));
        passwordTwo.setText(receivedSettings.getStringExtra("password"));
        phoneNumber.setText(receivedSettings.getStringExtra("number"));

        btnUpdate.setOnClickListener(v -> {

            teacherRequest.setName(name.getText().toString());
            teacherRequest.setEmail(email.getText().toString());
            teacherRequest.setPassword(passwordOne.getText().toString());
            sPasswordTwo = passwordTwo.getText().toString();
            teacherRequest.setNumber(phoneNumber.getText().toString());

            ValidateInput validateName =  new ValidateInput(teacherRequest.getName(), findViewById(R.id.boxName_settingsPersonalData));
            ValidateInput validateEmail =  new ValidateInput(teacherRequest.getEmail(), findViewById(R.id.boxEmail_settingsPersonalData));
            ValidateInput validatePassword =  new ValidateInput(teacherRequest.getPassword(), findViewById(R.id.boxPasswordOne_settingsPersonalData));
            ValidateInput validatePasswordTwo =  new ValidateInput(sPasswordTwo, findViewById(R.id.boxPasswordTwo_settingsPersonalData));
            ValidateInput validateNumber =  new ValidateInput(teacherRequest.getNumber(), findViewById(R.id.boxNumber_settingsPersonalData));


            if (!validateName.ValidateString() | !validateEmail.ValidateString() | !validatePassword.ValidatePassword(sPasswordTwo) | !validatePasswordTwo.ValidatePassword(teacherRequest.getPassword()) | !validateNumber.ValidateString()) {
                return;

            }  else {
                apiUpdataPersonalData(thisContext, idUser);
            }

        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void apiUpdataPersonalData(Context context, int auxIdUser) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updatePersonalData(auxIdUser, teacherRequest);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        Intent goToSettings = new Intent(context, Settings.class);
                        SettingsPersonalData.this.finish();
                        startActivity(goToSettings);
                        Toast.makeText(context, R.string.success_updataData, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, R.string.error_updataData, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
