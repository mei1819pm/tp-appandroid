package com.example.tp_appandroid.settings;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.TeacherModel;
import com.example.tp_appandroid.validate.ValidateInput;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsProfessionalData extends AppCompatActivity {

    private Context thisContext;

    private int idUser;

    private EditText workPlace;
    private Button btnUpdate;

    private TeacherModel teacherRequest = new TeacherModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_professional_data);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = SettingsProfessionalData.this;

        workPlace = (EditText) findViewById(R.id.workPlace_settingsProfessionalData);
        btnUpdate = (Button) findViewById(R.id.btnUpdate_settingsProfessionalData);

        Intent receivedSettings = getIntent();
        idUser = receivedSettings.getIntExtra("idUser", 0);
        workPlace.setText(receivedSettings.getStringExtra("workPlace"));

        btnUpdate.setOnClickListener(v -> {

            teacherRequest.setUniversity(workPlace.getText().toString());

            ValidateInput validateWorkPlace =  new ValidateInput(teacherRequest.getUniversity(), findViewById(R.id.boxWorkPlace_settingsProfessionalData));

            if (!validateWorkPlace.ValidateString()) {
                return;

            } else {
                apiUpdataProfessionalData(thisContext, idUser);
            }
        });
    }


    //Voltar Para Tras
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void apiUpdataProfessionalData(Context context, int auxIdUser) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateWorkPlace(auxIdUser, teacherRequest);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        Intent goToSettings = new Intent(context, Settings.class);
                        SettingsProfessionalData.this.finish();
                        startActivity(goToSettings);
                        Toast.makeText(context, R.string.success_updataData, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, R.string.error_updataData, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
