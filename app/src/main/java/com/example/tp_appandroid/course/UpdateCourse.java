package com.example.tp_appandroid.course;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.CourseModel;
import com.example.tp_appandroid.validate.ValidateInput;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateCourse extends AppCompatActivity {

    private Context thisContext;

    private CourseModel course;

    private EditText nameCourse;
    private Button btnUpdate;
    private int idCouse;

    private CourseModel courseResquest = new CourseModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_course);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent received = getIntent();
        Gson gson = new Gson();
        course = gson.fromJson(received.getStringExtra("courseUpdate"),CourseModel.class);

        nameCourse = (EditText) findViewById(R.id.course_updateCourse);
        btnUpdate = (Button) findViewById(R.id.btnUpdate_updateCourse);

        nameCourse.setText(course.getDescription());
        idCouse = course.getId();

        btnUpdate.setOnClickListener(v-> {
            courseResquest.setDescription(nameCourse.getText().toString());

            ValidateInput   validateCourse =  new ValidateInput(courseResquest.getDescription(), findViewById(R.id.boxCourse_updateCourse));

            if (!validateCourse.ValidateString() ) {
                return;

            } else {
                apiUpdataCourse();
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void apiUpdataCourse() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateCourse(idCouse, courseResquest);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        UpdateCourse.this.finish();
                        Toast.makeText(thisContext, R.string.success_updateCurse, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_updateCurse, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
