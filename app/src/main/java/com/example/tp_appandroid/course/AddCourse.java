package com.example.tp_appandroid.course;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.dialogs.HourPickerFragment;
import com.example.tp_appandroid.models.CourseModel;
import com.example.tp_appandroid.models.DisciplineModel;
import com.example.tp_appandroid.new_account.NewAccount;
import com.example.tp_appandroid.validate.ValidateInput;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCourse extends AppCompatActivity implements AdapterView.OnItemSelectedListener, TimePickerDialog.OnTimeSetListener {

    private Spinner dayWeekSpinner;
    private EditText course, discipline, acronym, hour;
    private RadioGroup radioGroup;
    private RadioButton semester;
    private Button btnAddCourse;

    private int idSemester;
    private String dayWeek;

    private int idSchoolYear;
    private SharedPreferences userSession;
    private int idUser;

    private DisciplineModel disciplineRequest = new DisciplineModel();
    private Context thisContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent getIntent = getIntent();
        idSchoolYear = getIntent.getIntExtra("idSchoolYear", 1);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        course = (EditText) findViewById(R.id.course_addCourse);
        discipline = (EditText) findViewById(R.id.discipline_addCourse);
        acronym = (EditText) findViewById(R.id.acronym_addCourse);
        hour = (EditText) findViewById(R.id.hour_addCourse);
        dayWeekSpinner = (Spinner) findViewById(R.id.day_addCourse);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupSemester_addCourse);
        btnAddCourse = (Button) findViewById(R.id.btnAddCourse_addCourse);

        ConstSpinner();


        hour.setOnClickListener(v-> {
                DialogFragment newFragment = new HourPickerFragment();
                newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        btnAddCourse.setOnClickListener(v -> {

            GetIdSemester();

            ValidateInput validateCourse =  new ValidateInput(course.getText().toString(), findViewById(R.id.boxCourse_addCourse));
            ValidateInput   validateDiscipline =  new ValidateInput(discipline.getText().toString(), findViewById(R.id.boxDiscipline_addCourse));
            ValidateInput   validateAcronym =  new ValidateInput(acronym.getText().toString(), findViewById(R.id.boxAcronym_addCourse));

            if (!validateCourse.ValidateString() | !validateDiscipline.ValidateString() | !validateAcronym.ValidateString()) {
                return;
            } else {

                disciplineRequest.setName(discipline.getText().toString());
                disciplineRequest.setAcronym(acronym.getText().toString());
                disciplineRequest.setNameCourse(course.getText().toString());
                disciplineRequest.setIdSchoolYear(idSchoolYear);
                disciplineRequest.setIdSemester(idSemester);
                disciplineRequest.setIdUser(idUser);
                String[] splitHour = hour.getText().toString().split(" ");
                disciplineRequest.setTimeAttendace(splitHour[0]);
                disciplineRequest.setDayAttendace(dayWeek);

                ApiAddCourse();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void ConstSpinner() {
        ArrayAdapter<CharSequence> adapterDayWeek = ArrayAdapter.createFromResource(this,
                R.array.days_week, android.R.layout.simple_spinner_item);
        adapterDayWeek.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dayWeekSpinner.setAdapter(adapterDayWeek);
        dayWeekSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        dayWeek = parent.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void GetIdSemester() {
        int radioId = radioGroup.getCheckedRadioButtonId();
        semester = findViewById(radioId);

        switch (semester.getText().toString()) {
            case "1º Semestre":
                idSemester = 1;
                break;
            case "2º Semestre":
                idSemester = 2;
                break;
            default:
                idSemester = 1;
                break;
        }
    }

    private void ApiAddCourse() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.addCourse(disciplineRequest);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res > 0) {
                        AddCourse.this.finish();
                        Toast.makeText(thisContext, R.string.success_addCurse, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_addCourse, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String auxMinute = minute + "";
        String auxHour = hourOfDay + "";

        if (hourOfDay < 10) {
            auxHour = "0" + auxHour;
        }

        if (minute < 10) {
            auxMinute = "0" + auxMinute;
        }

        hour.setText(auxHour + ":" + auxMinute + " H");
    }
}
