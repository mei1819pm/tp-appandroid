package com.example.tp_appandroid.email;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.EmailModel;
import com.example.tp_appandroid.validate.ValidateInput;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrepareEmail extends AppCompatActivity {

    private Context thisContext;
    private RadioGroup radioGroup;
    private RadioButton radioButtonSelect;

    private EditText emailAdministrative, emailSubject, emailBody;
    private Button btnSendEmai;

    private EmailModel dataEmail = new EmailModel();

    private SharedPreferences userSession;
    private int idUser;

    private  int aux = 0;

    private ProgressBar loadingSendEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepare_email);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = PrepareEmail.this;

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        Intent getIntent = getIntent();
        String auxsubjectEmail = getIntent.getStringExtra("subjectEmail");
        int auxDiscipline = getIntent.getIntExtra("discipline", 0);

        radioGroup = (RadioGroup) findViewById(R.id.radioGrupSend_prepareEmail);
        emailAdministrative = (EditText) findViewById(R.id.emailAdministrative_prepareEmail);
        emailSubject = (EditText) findViewById(R.id.emailSubject_prepareEmail);
        emailBody = (EditText) findViewById(R.id.emailBody_prepareEmail);
        btnSendEmai = (Button) findViewById(R.id.btnSendEmail_prepareEmail);

        loadingSendEmail = findViewById(R.id.loading_prepareEmail);

        emailSubject.setText(auxsubjectEmail);

        //Scroll no body do email
        emailBody.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (emailBody.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

        btnSendEmai.setOnClickListener(v -> {

            ValidateInput validateEmail =  new ValidateInput(emailBody.getText().toString(), findViewById(R.id.boxEmailBody_prepareEmail));
            ValidateInput validateSubject =  new ValidateInput(emailSubject.getText().toString(), findViewById(R.id.boxEmailSubject_prepareEmail));

            if (!validateEmail.ValidateString() | !validateSubject.ValidateString()) {
                return;
            } else {

                int radioId = radioGroup.getCheckedRadioButtonId();
                radioButtonSelect = findViewById(radioId);

                switch (radioButtonSelect.getText().toString()) {
                    case "Delegados":
                        dataEmail.setSendTo(1);
                        break;
                    case "Turma toda":
                        dataEmail.setSendTo(0);
                        break;
                    case "Nenhum":
                        aux = 1;
                        break;
                    default:
                        dataEmail.setSendTo(0);
                        break;
                }

                dataEmail.setId(idUser);
                dataEmail.setIdDiscipline(auxDiscipline);
                dataEmail.setEmailAdministrative(emailAdministrative.getText().toString());
                dataEmail.setSubject(emailSubject.getText().toString());
                dataEmail.setBody(emailBody.getText().toString());

                if(aux == 0) {
                    apiSendEmail();
                } else {
                    apiSendEmailAdministrative();
                }

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void apiSendEmailAdministrative() {
        loadingSendEmail.setVisibility(View.VISIBLE);
        btnSendEmai.setVisibility(View.GONE);

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.sendEmailAdministrative(dataEmail);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res == 1) {
                        loadingSendEmail.setVisibility(View.GONE);
                        Toast.makeText(thisContext, R.string.success_sendEmail, Toast.LENGTH_SHORT).show();
                        finish();

                    } else  {
                        loadingSendEmail.setVisibility(View.GONE);
                        btnSendEmai.setVisibility(View.VISIBLE);
                        Toast.makeText(thisContext, R.string.error_SendEmail, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void apiSendEmail() {
        loadingSendEmail.setVisibility(View.VISIBLE);
        btnSendEmai.setVisibility(View.GONE);

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.sendEmail(dataEmail);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res == 1) {
                        loadingSendEmail.setVisibility(View.GONE);
                        Toast.makeText(thisContext, R.string.success_sendEmail, Toast.LENGTH_SHORT).show();
                        finish();

                    } else  {
                        loadingSendEmail.setVisibility(View.GONE);
                        btnSendEmai.setVisibility(View.VISIBLE);
                        Toast.makeText(thisContext, R.string.error_SendEmail, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
