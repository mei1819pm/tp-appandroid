package com.example.tp_appandroid.calendar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.example.tp_appandroid.MainActivity;
import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.EventCalendarModel;
import com.example.tp_appandroid.planification.SchoolYears;
import com.example.tp_appandroid.settings.Settings;
import com.example.tp_appandroid.today.HomeApp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Calendar extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private SharedPreferences userSession;
    private CalendarView calendarView;
    List<EventDay> events = new ArrayList<>();
    private int idUser;
    private Context thisContext;
    List<EventCalendarModel> eventsCalendar = new ArrayList<>();

    private ProgressBar loadingCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        thisContext = Calendar.this;

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Colocar o azul a dizer que esta situado nesta pagina automaticamente por causa de serem atividades
        navigationView.setCheckedItem(R.id.nav_calendar);

        //Trabalhar no Calendario
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        calendarView.showCurrentMonthPage();

        //Carregar Calendario com Eventos
        apiGetEventsCalendar(idUser);

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                java.util.Calendar clickedDayCalendar = eventDay.getCalendar();
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                Intent goToCalendarEvents = new Intent(thisContext, CalendarEventsDay.class);
                goToCalendarEvents.putExtra("dateEvent", df.format(clickedDayCalendar.getTime()).toString());
                startActivity(goToCalendarEvents);
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_today:
                Intent intentHome = new Intent(Calendar.this, HomeApp.class);
                startActivity(intentHome);
                break;

            case R.id.nav_planification:
                Intent intentPlanificationCourse = new Intent(Calendar.this, SchoolYears.class);
                startActivity(intentPlanificationCourse);

                break;

            case R.id.nav_settings:
                Intent intentSettings = new Intent(Calendar.this, Settings.class);
                startActivity(intentSettings);
                break;

            case R.id.nav_logout:
                SharedPreferences.Editor editor = userSession.edit();
                editor.clear();
                editor.commit();

                Intent intentGoLogin = new Intent(Calendar.this, MainActivity.class);
                this.finish();
                startActivity(intentGoLogin);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    public void apiGetEventsCalendar(int auxIdUser) {

        loadingCalendar = findViewById(R.id.loading_calendar);

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<EventCalendarModel>> cal = service.getAllEventsTwoYears(auxIdUser);
        cal.enqueue(new Callback<List<EventCalendarModel>>() {
            @Override
            public void onResponse(Call<List<EventCalendarModel>> call, Response<List<EventCalendarModel>> response) {


                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        eventsCalendar = response.body();

                        for (EventCalendarModel auxEvent : eventsCalendar) {

                            String[] itemsDate = auxEvent.getDate().split("/");

                            java.util.Calendar calendarDay = new GregorianCalendar();

                            calendarDay.set(Integer.parseInt(itemsDate[2]), Integer.parseInt(itemsDate[1]) - 1, Integer.parseInt(itemsDate[0]), 00, 00);

                            switch (auxEvent.getTypeEvent()) {
                                case 1:
                                    events.add(new EventDay(calendarDay, R.drawable.class_circle));
                                    break;

                                case 2:
                                    events.add(new EventDay(calendarDay, R.drawable.jobdelivery_circle));
                                    break;

                                case 3:
                                    events.add(new EventDay(calendarDay, R.drawable.contacthour_circle));
                                    break;

                                case 4:
                                    events.add(new EventDay(calendarDay, R.drawable.class_job_contacthour_circle));
                                    break;

                                case 5:
                                    events.add(new EventDay(calendarDay, R.drawable.class_jobdelivery_circle));
                                    break;

                                case 6:
                                    events.add(new EventDay(calendarDay, R.drawable.class_contacthour_circle));
                                    break;

                                case 7:
                                    events.add(new EventDay(calendarDay, R.drawable.jobdelivery_contacthour_circle));
                                    break;
                            }

                        }

                        calendarView.setEvents(events);
                        loadingCalendar.setVisibility(View.GONE);

                    } else {
                        Toast.makeText(thisContext, R.string.other_noEventsCalendar, Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<EventCalendarModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


}
