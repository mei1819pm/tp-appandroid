package com.example.tp_appandroid.calendar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterTodayClass;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterTodayContact;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterTodayJob;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.models.ContactHourModel;
import com.example.tp_appandroid.models.JobDeliveryModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarEventsDay extends AppCompatActivity {

    private Context thisContext;
    private SharedPreferences userSession;
    private int idUser;

    private String dateEvents;

    private RecyclerView recyclerViewEventsClass;
    private List<ClassModel> listClass = new ArrayList<>();
    private RecyclerViewAdapterTodayClass adapterEventsClassDay;
    RecyclerView.LayoutManager layoutManagerEventsClassDay;

    private RecyclerView recyclerViewEventsJob;
    private List<JobDeliveryModel> listJobs = new ArrayList<>();
    private RecyclerViewAdapterTodayJob adapterEventsJobDay;
    RecyclerView.LayoutManager layoutManagerEventsJobDay;

    private RecyclerView recyclerViewEventsContactHour;
    private List<ContactHourModel> listContactHour = new ArrayList<>();
    private RecyclerViewAdapterTodayContact adapterEventsContactHour;
    RecyclerView.LayoutManager layoutManagerEventsContactHourDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_events_day);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent receivedCalendar = getIntent();
        dateEvents = receivedCalendar.getStringExtra("dateEvent");
        setTitle(dateEvents);

        thisContext = CalendarEventsDay.this;

        userSession = getSharedPreferences("userSession_app",MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        ConstRecyclerViewClass();
        ConstRecyclerViewJob();
        ConstRecyclerViewContactHour();

        apiGetEventsClass(idUser, dateEvents);
        apiGetEventsJob(idUser, dateEvents);
        apiGetContactHour(idUser, dateEvents);
    }

    //Voltar Para Tras
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void apiGetEventsClass(int auxIdUser, String auxDate) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);
        Call<List<ClassModel>> cal = service.getClassByDay(auxIdUser,  auxDate.replace("/","-"));
        cal.enqueue(new Callback<List<ClassModel>>() {
            @Override
            public void onResponse(Call<List<ClassModel>> call, Response<List<ClassModel>> response) {

                if(response.code() == 200) {

                    if(response.body().size() > 0) {
                        listClass = response.body();
                        ConstRecyclerViewClass();
                        findViewById(R.id.classEmpty_calendar).setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<List<ClassModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void apiGetEventsJob(int auxIdUser, String auxDate) {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<JobDeliveryModel>> call = service.getJobDeliveryByDay(auxIdUser, auxDate.replace("/","-"));
        call.enqueue(new Callback<List<JobDeliveryModel>>() {
            @Override
            public void onResponse(Call<List<JobDeliveryModel>> call, Response<List<JobDeliveryModel>> response) {
                if(response.code() == 200) {


                    if(response.body().size() > 0) {
                        listJobs = response.body();
                        ConstRecyclerViewJob();
                        findViewById(R.id.jobDeliveryEmpty_calendar).setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<JobDeliveryModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void apiGetContactHour(int auxIdUser, String auxDate) {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<ContactHourModel>> cal = service.getContactHourByDay(auxIdUser, auxDate.replace("/","-"));
        cal.enqueue(new Callback<List<ContactHourModel>>() {
            @Override
            public void onResponse(Call<List<ContactHourModel>> call, Response<List<ContactHourModel>> response) {
                if(response.code() == 200) {

                    if(response.body().size() > 0) {
                        listContactHour = response.body();
                        ConstRecyclerViewContactHour();
                        findViewById(R.id.contactHourEmpty_calendar).setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ContactHourModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ConstRecyclerViewClass() {
        recyclerViewEventsClass = (RecyclerView) findViewById(R.id.recyclerview_calendar_class);
        layoutManagerEventsClassDay = new LinearLayoutManager(thisContext);
        recyclerViewEventsClass.setLayoutManager(layoutManagerEventsClassDay);
        adapterEventsClassDay = new RecyclerViewAdapterTodayClass(this, listClass);
        recyclerViewEventsClass.setAdapter(adapterEventsClassDay);
    }

    private void ConstRecyclerViewJob() {
        recyclerViewEventsJob = (RecyclerView) findViewById(R.id.recyclerview_calendar_jobDelivey);
        layoutManagerEventsJobDay = new LinearLayoutManager(thisContext);
        recyclerViewEventsJob.setLayoutManager(layoutManagerEventsJobDay);
        adapterEventsJobDay = new RecyclerViewAdapterTodayJob(thisContext, listJobs);
        recyclerViewEventsJob.setAdapter(adapterEventsJobDay);
    }

    private void ConstRecyclerViewContactHour() {
        recyclerViewEventsContactHour = (RecyclerView) findViewById(R.id.recyclerview_calendar_contactHour);
        layoutManagerEventsContactHourDay = new LinearLayoutManager(thisContext);
        recyclerViewEventsContactHour.setLayoutManager(layoutManagerEventsContactHourDay);
        adapterEventsContactHour = new RecyclerViewAdapterTodayContact(thisContext, listContactHour);
        recyclerViewEventsContactHour.setAdapter(adapterEventsContactHour);
    }

}
