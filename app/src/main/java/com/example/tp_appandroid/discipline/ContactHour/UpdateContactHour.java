package com.example.tp_appandroid.discipline.ContactHour;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.dialogs.DatePickerFragment;
import com.example.tp_appandroid.dialogs.HourPickerFragment;
import com.example.tp_appandroid.models.ContactHourModel;
import com.example.tp_appandroid.validate.ValidateInput;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateContactHour extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private Context thisContext;

    private ContactHourModel contactHour;

    private EditText day, hour, description, appRoom;
    private TextView student;
    private Button btnUpdate;

    private RadioGroup radioGroup;
    private RadioButton rbType;

    private RadioButton typeOnline, typePresential;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_contact_hour);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent received = getIntent();
        Gson gson = new Gson();
        contactHour = gson.fromJson(received.getStringExtra("contactHourUpdate"), ContactHourModel.class);

        student = (TextView) findViewById(R.id.student_updateContactHour);
        description = (EditText) findViewById(R.id.description_updateContactHour);
        appRoom = (EditText) findViewById(R.id.appRoom_updateContactHour);
        day = (EditText) findViewById(R.id.day_updateContactHour);
        hour = (EditText) findViewById(R.id.hour_updateContactHour);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupType_updateContactHour);
        typeOnline = (RadioButton) findViewById(R.id.rbOnline_updateContactHour);
        typePresential = (RadioButton) findViewById(R.id.rbPresential_updateContactHour);
        btnUpdate = (Button) findViewById(R.id.btnUpdateContactHour_updateContactHour);

        student.setText(contactHour.getNameStudent());
        description.setText(contactHour.getDescription());
        appRoom.setText(contactHour.getAppOrRoom());
        day.setText(contactHour.getDay());
        hour.setText(contactHour.getHour() + " H");

        if(contactHour.getType().equals("Presencial")) {
            typePresential.setChecked(true);
        } else {
            typeOnline.setChecked(true);
        }

        day.setOnClickListener(v-> {
            DialogFragment datePicker = new DatePickerFragment();
            datePicker.show(getSupportFragmentManager(), "date picker");
        });

        hour.setOnClickListener(v-> {
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        btnUpdate.setOnClickListener(v-> {

            GetType();
            ValidateInput validateDescription =  new ValidateInput(description.getText().toString(), findViewById(R.id.boxDescription_updateContactHour));
            ValidateInput   validateAppRoom =  new ValidateInput(appRoom.getText().toString(), findViewById(R.id.boxAppRoom_updateContactHour));

            if (!validateDescription.ValidateString() | !validateAppRoom.ValidateString()) {
                return;

            }   else {

                contactHour.setDescription(description.getText().toString());
                contactHour.setAppOrRoom(appRoom.getText().toString());
                String[] splitHour = hour.getText().toString().split(" ");
                contactHour.setHour(splitHour[0]);
                contactHour.setDay(day.getText().toString());

                ApiUpdateContactHour();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void GetType() {
        int radioId = radioGroup.getCheckedRadioButtonId();
        rbType = findViewById(radioId);

        contactHour.setType(rbType.getText().toString());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        day.setText(dayOfMonth + "/" + (month+1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String auxMinute = minute + "";
        String auxHour = hourOfDay + "";

        if (hourOfDay < 10) {
            auxHour = "0" + auxHour;
        }

        if (minute < 10) {
            auxMinute = "0" + auxMinute;
        }

        hour.setText(auxHour + ":" + auxMinute + " H");
    }

    private void ApiUpdateContactHour() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateContactHour(contactHour.getId(), contactHour);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        UpdateContactHour.this.finish();
                        Toast.makeText(thisContext, R.string.success_updateContactHour, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_updateContactHour, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
