package com.example.tp_appandroid.discipline.Class;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.dialogs.DatePickerFragment;
import com.example.tp_appandroid.dialogs.HourPickerFragment;
import com.example.tp_appandroid.discipline.AddDiscipline;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.models.StudentModel;
import com.example.tp_appandroid.validate.ValidateInput;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddClass extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private Integer idDiscipline = -1;

    private SharedPreferences userSession;
    private int idUser;

    private Context thisContext;

    private Button btnAdd;
    private EditText number, room, hourStart, hourEnd, day, type;

    private ClassModel classModel = new ClassModel();

    private int controllerHour = 0; // 0 -> Hora de inicio, 1 -> hora de termino

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);

        thisContext = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Adicionar Aula");

        Intent getIntent = getIntent();
        idDiscipline = getIntent.getIntExtra("idDiscipline", 1);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        ApiGetClassNumber();

        number = (EditText) findViewById(R.id.number_addClass);
        room = (EditText) findViewById(R.id.room_addClass);
        hourStart = (EditText) findViewById(R.id.hourStart_addClass);
        hourEnd = (EditText) findViewById(R.id.hourEnd_addClass);
        day = (EditText) findViewById(R.id.day_addClass);
        type = (EditText) findViewById(R.id.type_addClass);

        day.setOnClickListener(v-> {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "DatePicker");
        });

        hourStart.setOnClickListener(v-> {
            controllerHour = 0;
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        hourEnd.setOnClickListener(v-> {
            controllerHour = 1;
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        getCurrenteDate();

        btnAdd = (Button) findViewById(R.id.btnAddClass_addClass);

        btnAdd.setOnClickListener(v -> {
            ValidateInput validateNumber = new ValidateInput(number.getText().toString(), findViewById(R.id.boxNumber_addClass));
            ValidateInput validateRoom = new ValidateInput(room.getText().toString(), findViewById(R.id.boxRoom_addClass));
            ValidateInput validateType = new ValidateInput(type.getText().toString(), findViewById(R.id.boxtype_addClass));

            if (!validateNumber.ValidateString() | !validateRoom.ValidateString() | !validateType.ValidateString()) {
                return;
            } else {
                classModel.setIdDiscipline(idDiscipline);
                classModel.setNumber(Integer.parseInt(number.getText().toString()));
                classModel.setClassRoom(room.getText().toString());
                classModel.setDay(day.getText().toString());
                String[] splitHourStart = hourStart.getText().toString().split(" ");
                classModel.setStart(splitHourStart[0]);
                String[] splitHourEnd = hourEnd.getText().toString().split(" ");
                classModel.setEnd(splitHourEnd[0]);
                classModel.setTypeClass(type.getText().toString());

                ApiAddClass();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getCurrenteDate() {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);

        day.setText(dd +"/"+ (mm + 1) + "/" + yy);
    }

    private void ApiGetClassNumber() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.getCurrentLessonNumberForDiscipline(idDiscipline);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        number.setText(response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ApiAddClass() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.addClass(classModel);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res > 0) {
                        AddClass.this.finish();
                        Toast.makeText(thisContext, R.string.success_addClass, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_addClass, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        day.setText(dayOfMonth + "/" + (month+1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String auxMinute = minute + "";
        String auxHour = hourOfDay + "";

        if (hourOfDay < 10) {
            auxHour = "0" + auxHour;
        }

        if (minute < 10) {
            auxMinute = "0" + auxMinute;
        }

        if(controllerHour == 0) {
            hourStart.setText(auxHour + ":" + auxMinute + " H");
        } else {
            hourEnd.setText(auxHour + ":" + auxMinute + " H");
        }

    }
}
