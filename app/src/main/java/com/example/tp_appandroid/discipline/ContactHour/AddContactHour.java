package com.example.tp_appandroid.discipline.ContactHour;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.dialogs.DatePickerFragment;
import com.example.tp_appandroid.dialogs.HourPickerFragment;
import com.example.tp_appandroid.discipline.Student.SpinnerAdapterStudent;
import com.example.tp_appandroid.models.ContactHourModel;
import com.example.tp_appandroid.models.StudentModel;
import com.example.tp_appandroid.validate.ValidateInput;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddContactHour extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private Context thisContext;
    private int idDiscipline;

    private SharedPreferences userSession;
    private int idUser;

    private Spinner spinnerStudent;
    private SpinnerAdapterStudent adapterStudent;

    private List<StudentModel> listStudent = new ArrayList<>();
    private int idStudent;

    private RadioGroup radioGroup;
    private RadioButton rbType;

    private EditText description, appRoom;
    private Button btnAdd;
    private EditText day, hour;

    private  int positionSpinner;
    private ContactHourModel contactHour = new ContactHourModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact_hour);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent getIntent = getIntent();
        idDiscipline = getIntent.getIntExtra("idDiscipline", 1);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        spinnerStudent = (Spinner) findViewById(R.id.spinnerStudent_addContactHour);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupType_addContactHour);
        description = (EditText) findViewById(R.id.description_addContactHour);
        appRoom = (EditText) findViewById(R.id.appRoom_addContactHour) ;
        day = (EditText) findViewById(R.id.day_addContactHour);
        hour = (EditText) findViewById(R.id.hour_addContactHour);
        btnAdd = (Button) findViewById(R.id.btnAddContactHour_addContactHour);

        listStudent.add(new StudentModel(0, 0, "Selecione o Aluno","","",0,0,0));

        ApiGetStudentByDiscipline();

        ConstSpinner();

        getCurrenteDate();

        day.setOnClickListener(v-> {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "DatePicker");
        });

        hour.setOnClickListener(v-> {
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        btnAdd.setOnClickListener(v-> {

           GetType();
            ValidateInput validateDescription =  new ValidateInput(description.getText().toString(), findViewById(R.id.boxDescription_addContactHour));
            ValidateInput validateAppRoom =  new ValidateInput(appRoom.getText().toString(), findViewById(R.id.boxAppRoom_addContactHour));

            if (positionSpinner == 0) {
                Toast.makeText(thisContext, R.string.error_getStudentInsert, Toast.LENGTH_SHORT).show();

            } else if (!validateDescription.ValidateString() | !validateAppRoom.ValidateString()) {
                return;

            }   else {

                contactHour.setIdDiscipline(idDiscipline);
                contactHour.setIdStudent(idStudent);
                contactHour.setIdTeacher(idUser);
                contactHour.setDescription(description.getText().toString());
                contactHour.setAppOrRoom(appRoom.getText().toString());
                String[] splitHour = hour.getText().toString().split(" ");
                contactHour.setHour(splitHour[0]);
                contactHour.setDay(day.getText().toString());

                ApiAddContactHour();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void ConstSpinner() {
        adapterStudent = new SpinnerAdapterStudent(thisContext, android.R.layout.simple_list_item_1, listStudent);

        spinnerStudent.setAdapter(adapterStudent);
        spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                StudentModel student = adapterStudent.getItem(position);
                idStudent = student.getId();

                positionSpinner = position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void GetType() {
        int radioId = radioGroup.getCheckedRadioButtonId();
        rbType = findViewById(radioId);

        contactHour.setType(rbType.getText().toString());
    }

    public void getCurrenteDate() {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);

        day.setText(dd +"/"+ (mm + 1) + "/" + yy);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        day.setText(dayOfMonth + "/" + (month+1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String auxMinute = minute + "";
        String auxHour = hourOfDay + "";

        if (hourOfDay < 10) {
            auxHour = "0" + auxHour;
        }

        if (minute < 10) {
            auxMinute = "0" + auxMinute;
        }

        hour.setText(auxHour + ":" + auxMinute + " H");
    }

    private void ApiGetStudentByDiscipline() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<StudentModel>> call = service.getStudentByDiscipline(idDiscipline);
        call.enqueue(new Callback<List<StudentModel>>() {
            @Override
            public void onResponse(Call<List<StudentModel>> call, Response<List<StudentModel>> response) {
                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        listStudent.addAll(response.body());
                        ConstSpinner();
                    } else {
                        Toast.makeText(thisContext, R.string.error_getStudentInsert, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<StudentModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ApiAddContactHour() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.addContactHour(contactHour);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res > 0) {
                        AddContactHour.this.finish();
                        Toast.makeText(thisContext, R.string.success_addContactHour, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_addContactHour, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
