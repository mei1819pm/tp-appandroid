package com.example.tp_appandroid.discipline.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterJobDelivery;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.discipline.JobDelivery.AddJobDelivery;
import com.example.tp_appandroid.discipline.JobDelivery.UpDateJobDelivery;
import com.example.tp_appandroid.models.JobDeliveryModel;
import com.example.tp_appandroid.planification.Discipline;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentJobDeliveryDiscipline extends Fragment {


    private View v;
    public FragmentJobDeliveryDiscipline() {
        // Required empty public constructor
    }
    private RecyclerView recyclerViewJobDelivery;
    private List<JobDeliveryModel> listJobDelivery = new ArrayList<>();

    private int idDiscipline = 0;

    private int showMassage = 1;

    private FloatingActionButton btnAdd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_fragment_job_delivery_discipline, container, false);

        if(showMassage == 0) {
            GoneMessage();
        }

        ConstRecyclerView();

        btnAdd = (FloatingActionButton) v.findViewById(R.id.btnAdd_jobDeliveryDiscipline);

        btnAdd.setOnClickListener(v-> {
            Intent goToAddJobDelivery = new Intent(getContext(), AddJobDelivery.class);
            goToAddJobDelivery.putExtra("idDiscipline", idDiscipline);
            startActivity(goToAddJobDelivery);
        });

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GetIdDiscipline();
        apiGetJobDelivey();
    }

    @Override
    public void onResume() {
        super.onResume();
       apiGetJobDelivey();
    }


    private void GetIdDiscipline(){
        Discipline activity = (Discipline) getActivity();
        idDiscipline = activity.GetIdDiscipline();
    }

    private void ConstRecyclerView() {

        recyclerViewJobDelivery = (RecyclerView) v.findViewById(R.id.recyclerview_jobDeliveryDiscipline);
        RecyclerViewAdapterJobDelivery adpater = new RecyclerViewAdapterJobDelivery(getContext(), listJobDelivery);
        recyclerViewJobDelivery.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewJobDelivery.setAdapter(adpater);

        adpater.setOnItemClickListener(new RecyclerViewAdapterJobDelivery.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                Gson gson = new Gson();

                Intent goToUpdateJobDelivery = new Intent(getContext(), UpDateJobDelivery.class);
                goToUpdateJobDelivery.putExtra("jobDeliveryUpdate", gson.toJson(listJobDelivery.get(position)));
                startActivity(goToUpdateJobDelivery);
            }

            @Override
            public void onItemLongClick(View v, int position) {

            }

        });
    }

    private void apiGetJobDelivey() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<JobDeliveryModel>> call = service.getJobDeliveryByDiscipline(idDiscipline);
        call.enqueue(new Callback<List<JobDeliveryModel>>() {
            @Override
            public void onResponse(Call<List<JobDeliveryModel>> call, Response<List<JobDeliveryModel>> response) {
                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        listJobDelivery = response.body();
                        ConstRecyclerView();
                        showMassage = 0;
                        GoneMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<JobDeliveryModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void GoneMessage() {
        TextView empty = v.findViewById(R.id.jobEmpty_jobDeliveryDiscipline);
        empty.setVisibility(View.GONE);
    }
}
