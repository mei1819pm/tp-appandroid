package com.example.tp_appandroid.discipline.Student;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.StudentModel;
import com.example.tp_appandroid.validate.ValidateInput;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStudent extends AppCompatActivity {

    private Context thisContext;
    private int idDiscipline;

    private SharedPreferences userSession;
    private int idUser;

    private List<StudentModel> listStudent = new ArrayList<>();

    private Spinner spinnerStudent;
    private SpinnerAdapterStudent adapterStudent;

    private Button btnAdd;
    private EditText name, number, email;

    private int idStudent;

    private int typeQuery = -1;  /*0 inserir apenas estudante a disciplina - 1 inserir todo o estudante*/

    private RadioGroup radioGroup;
    private RadioButton rbDelegate;
    private int valueDelegate;

    private StudentModel studentRequest = new StudentModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent getIntent = getIntent();
        idDiscipline = getIntent.getIntExtra("idDiscipline", 1);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        spinnerStudent = (Spinner) findViewById(R.id.spinnerStudent_addStudent);
        name = (EditText) findViewById(R.id.name_addStudent);
        number = (EditText) findViewById(R.id.number_addStudent);
        email = (EditText) findViewById(R.id.email_addStudent);
        btnAdd = (Button) findViewById(R.id.btnAddStudent_addStudent);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroupDelegate_addStudent);

        listStudent.add(new StudentModel(0, 0, "Selecione o Aluno","","",0,0,0));

        ApiGetStudentInsertByTeacher();

        ConstSpinner();

        CheckChangeNumber();
        CheckChangeName();
        CheckChangeEmail();

        btnAdd.setOnClickListener(v-> {
            GetDelegate();

            studentRequest.setIdDiscipline(idDiscipline);
            studentRequest.setId(idStudent);
            studentRequest.setDelegate(valueDelegate);

           switch (typeQuery) {
               case 0:
                   apiAddStudenttoDisicpline();
                   break;

               case 1:

                   ValidateInput validateNumber =  new ValidateInput(number.getText().toString(), findViewById(R.id.boxNumber_addStudent));
                   ValidateInput validateName =  new ValidateInput(name.getText().toString(), findViewById(R.id.boxName_addStudent));
                   ValidateInput validateEmail =  new ValidateInput(email.getText().toString(), findViewById(R.id.boxEmail_addStudent));

                   if (!validateNumber.ValidateString() | !validateName.ValidateString() | !validateEmail.ValidateString()) {
                       return;
                   } else {

                       studentRequest.setNumber(Integer.parseInt(number.getText().toString()));
                       studentRequest.setName(name.getText().toString());
                       studentRequest.setEmail(email.getText().toString());
                       studentRequest.setIdTeacher(idUser);

                       apiAddStudent();
                   }
                   break;
                   default:
                       Toast.makeText(thisContext, R.string.error_noSelectInsertStudent, Toast.LENGTH_SHORT).show();
                       break;
           }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void CheckChangeNumber() {
        number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(count == 0) {
                    spinnerStudent.setEnabled(true);
                } else {
                    spinnerStudent.setEnabled(false);
                    typeQuery = 1;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void CheckChangeName() {
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(count == 0) {
                    spinnerStudent.setEnabled(true);
                } else {
                    spinnerStudent.setEnabled(false);
                    typeQuery = 1;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void CheckChangeEmail() {
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count == 0) {
                    spinnerStudent.setEnabled(true);
                } else {
                    spinnerStudent.setEnabled(false);
                    typeQuery = 1;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void ConstSpinner() {
        adapterStudent = new SpinnerAdapterStudent(thisContext, android.R.layout.simple_list_item_1, listStudent);

        spinnerStudent.setAdapter(adapterStudent);
        spinnerStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                StudentModel student = adapterStudent.getItem(position);
                idStudent = student.getId();

                if (position  != 0) {
                    number.setFocusable(false);
                    name.setFocusable(false);
                    email.setFocusable(false);

                    typeQuery = 0;
                } else {
                    number.setFocusableInTouchMode(true);
                    name.setFocusableInTouchMode(true);
                    email.setFocusableInTouchMode(true);
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void GetDelegate() {
        int radioId = radioGroup.getCheckedRadioButtonId();
        rbDelegate = findViewById(radioId);

        switch (rbDelegate.getText().toString()) {
            case "Aluno":
                valueDelegate = 0;
                break;
            case "Delegado":
                valueDelegate = 1;
                break;
            default:
                valueDelegate = 0;
                break;
        }
    }

    private void ApiGetStudentInsertByTeacher() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<StudentModel>> call = service.getStudentInsert(idUser);
        call.enqueue(new Callback<List<StudentModel>>() {
            @Override
            public void onResponse(Call<List<StudentModel>> call, Response<List<StudentModel>> response) {
                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        listStudent.addAll(response.body());
                        ConstSpinner();
                    } else {
                        Toast.makeText(thisContext, R.string.error_getStudentInsert, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<StudentModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void apiAddStudenttoDisicpline() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.addStudentDiscipline(studentRequest);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res > 0) {
                        AddStudent.this.finish();
                        Toast.makeText(thisContext, R.string.success_addStudent, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_addStudent, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void apiAddStudent() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.addStudent(studentRequest);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res > 0) {
                        AddStudent.this.finish();
                        Toast.makeText(thisContext, R.string.success_addStudent, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_addStudent, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
