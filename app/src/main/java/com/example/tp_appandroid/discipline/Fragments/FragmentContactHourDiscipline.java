package com.example.tp_appandroid.discipline.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterContactHour;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.discipline.ContactHour.AddContactHour;
import com.example.tp_appandroid.discipline.ContactHour.UpdateContactHour;
import com.example.tp_appandroid.models.ContactHourModel;
import com.example.tp_appandroid.planification.Discipline;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentContactHourDiscipline extends Fragment {


    public FragmentContactHourDiscipline() {
        // Required empty public constructor
    }

    private View v;

    private int idDiscipline;

    private RecyclerView recyclerViewContactHour;
    private List<ContactHourModel> listContactHour = new ArrayList<>();

    private int showMassage = 1;

    private  RecyclerViewAdapterContactHour adpater;

    private FloatingActionButton btnAdd;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_fragment_contact_hour_discipline, container, false);

        if(showMassage == 0) {
            GoneMessage();
        }

        ConstRecyclerView();

        btnAdd = (FloatingActionButton) v.findViewById(R.id.btnAdd_contactHourDiscipline);

        btnAdd.setOnClickListener(v-> {
            Intent goToAddContactHour = new Intent(getContext(), AddContactHour.class);
            goToAddContactHour.putExtra("idDiscipline", idDiscipline);
            startActivity(goToAddContactHour);
        });

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GetIdDiscipline();
        apiGetContactHour();
    }

    @Override
    public void onResume() {
        super.onResume();
        apiGetContactHour();
    }


    private void GetIdDiscipline(){
        Discipline activity = (Discipline) getActivity();
        idDiscipline = activity.GetIdDiscipline();
    }

    private void ConstRecyclerView() {

        recyclerViewContactHour = (RecyclerView) v.findViewById(R.id.recyclerview_contactHourDiscipline);
         adpater = new RecyclerViewAdapterContactHour(getContext(), listContactHour);
        recyclerViewContactHour.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewContactHour.setAdapter(adpater);

        adpater.setOnItemClickListener(new RecyclerViewAdapterContactHour.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                Gson gson = new Gson();

                Intent goToUpdateContactHour = new Intent(getContext(), UpdateContactHour.class);
                goToUpdateContactHour.putExtra("contactHourUpdate", gson.toJson(listContactHour.get(position)));
                startActivity(goToUpdateContactHour);
            }

            @Override
            public void onItemLongClick(View v, int position) {

            }

        });
    }

    private void apiGetContactHour() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<ContactHourModel>> call = service.getContactHourByDiscipline(idDiscipline);
        call.enqueue(new Callback<List<ContactHourModel>>() {
            @Override
            public void onResponse(Call<List<ContactHourModel>> call, Response<List<ContactHourModel>> response) {
                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        listContactHour = response.body();
                        ConstRecyclerView();
                        showMassage = 0;
                        GoneMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ContactHourModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void GoneMessage() {
        TextView empty = v.findViewById(R.id.contactHourEmpty_contactHourDiscipline);
        empty.setVisibility(View.GONE);
    }



}
