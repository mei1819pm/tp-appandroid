package com.example.tp_appandroid.discipline;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.dialogs.HourPickerFragment;
import com.example.tp_appandroid.models.CourseModel;
import com.example.tp_appandroid.models.DisciplineModel;
import com.example.tp_appandroid.validate.ValidateInput;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateDiscipline extends AppCompatActivity implements AdapterView.OnItemSelectedListener, TimePickerDialog.OnTimeSetListener {

    private Context thisContext;

    private DisciplineModel disciplineModel;

    private Spinner dayWeekSpinner;
    private EditText discipline, acronym, hour;
    private Button btnUpdateDiscipline;

    private int positionOption;
    private int startPositionSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_discipline);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent received = getIntent();
        Gson gson = new Gson();
        disciplineModel = gson.fromJson(received.getStringExtra("disciplineUpdate"),DisciplineModel.class);

        discipline = (EditText) findViewById(R.id.discipline_updateDiscipline);
        acronym = (EditText) findViewById(R.id.acronym_updateDiscipline);
        hour = (EditText) findViewById(R.id.hour_updateDiscipline);
        dayWeekSpinner = (Spinner) findViewById(R.id.day_updateDiscipline);

        btnUpdateDiscipline = (Button) findViewById(R.id.btnUpdateDisciplie_updateDiscipline);

        ConstSpinner();

        discipline.setText(disciplineModel.getName());
        acronym.setText(disciplineModel.getAcronym());
        hour.setText(disciplineModel.getTimeAttendace() + " H");
        getStartPositionSpinner();

        hour.setOnClickListener(v-> {
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        btnUpdateDiscipline.setOnClickListener(v-> {
            disciplineModel.setName(discipline.getText().toString());
            disciplineModel.setAcronym(acronym.getText().toString());
            String[] splitHour = hour.getText().toString().split(" ");
            disciplineModel.setTimeAttendace(splitHour[0]);


            ValidateInput validateDiscipline =  new ValidateInput(disciplineModel.getName(), findViewById(R.id.boxDiscipline_updateDiscipline));
            ValidateInput validateAcronym =  new ValidateInput(disciplineModel.getAcronym(), findViewById(R.id.boxAcronym_updateDiscipline));

            if (!validateDiscipline.ValidateString() || !validateAcronym.ValidateString() ) {
                return;

            } else {
                apiUpdateCourse();
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void apiUpdateCourse() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateDiscipline(disciplineModel.getId(), disciplineModel);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        UpdateDiscipline.this.finish();
                        Toast.makeText(thisContext, R.string.success_updateDiscipline, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_updateDiscipline, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void ConstSpinner() {
        ArrayAdapter<CharSequence> adapterDayWeek = ArrayAdapter.createFromResource(thisContext,
                R.array.days_week, android.R.layout.simple_spinner_item);
        adapterDayWeek.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dayWeekSpinner.setAdapter(adapterDayWeek);
        dayWeekSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String auxMinute = minute + "";
        String auxHour = hourOfDay + "";

        if (hourOfDay < 10) {
            auxHour = "0" + auxHour;
        }

        if (minute < 10) {
            auxMinute = "0" + auxMinute;
        }

        hour.setText(auxHour + ":" + auxMinute + " H");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (positionOption == 0) {
            parent.setSelection(startPositionSpinner);
            positionOption = 1;
        }

        disciplineModel.setDayAttendace(parent.getSelectedItem().toString());
        positionOption = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getStartPositionSpinner() {
        List<String> arraySpinner = Arrays.asList(getResources().getStringArray(R.array.days_week));

        for (String aux : arraySpinner) {
            if (aux.equals(disciplineModel.getDayAttendace())) {
                startPositionSpinner = arraySpinner.indexOf(aux);
                break;
            }
        }
    }
}
