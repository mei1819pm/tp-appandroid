package com.example.tp_appandroid.discipline.Class;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.dialogs.DatePickerFragment;
import com.example.tp_appandroid.dialogs.DialogAlert;
import com.example.tp_appandroid.dialogs.HourPickerFragment;
import com.example.tp_appandroid.email.PrepareEmail;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.validate.ValidateInput;
import com.google.gson.Gson;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateClass extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private ClassModel classModel;

    private SharedPreferences userSession;
    private int idUser;

    private Context thisContext;

    private Button btnAdd;
    private EditText number, room, hourStart, hourEnd, day, type;

    private int controllerHour = 0;

    private DialogAlert alert = new DialogAlert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);

        thisContext = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Editar Aula");

        Intent received = getIntent();
        Gson gson = new Gson();
        classModel = gson.fromJson(received.getStringExtra("class"), ClassModel.class);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        number = (EditText) findViewById(R.id.number_addClass);
        room = (EditText) findViewById(R.id.room_addClass);
        hourStart = (EditText) findViewById(R.id.hourStart_addClass);
        hourEnd = (EditText) findViewById(R.id.hourEnd_addClass);
        day = (EditText) findViewById(R.id.day_addClass);
        type = (EditText) findViewById(R.id.type_addClass);

        day.setOnClickListener(v-> {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "DatePicker");
        });

        hourStart.setOnClickListener(v-> {
            controllerHour = 0;
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        hourEnd.setOnClickListener(v-> {
            controllerHour = 1;
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        btnAdd = (Button) findViewById(R.id.btnAddClass_addClass);

        btnAdd.setText("Guardar Alterações");

        btnAdd.setOnClickListener(v -> {
            ValidateInput validateNumber = new ValidateInput(number.getText().toString(), findViewById(R.id.boxNumber_addClass));
            ValidateInput validateRoom = new ValidateInput(room.getText().toString(), findViewById(R.id.boxRoom_addClass));
            ValidateInput validateType = new ValidateInput(type.getText().toString(), findViewById(R.id.boxtype_addClass));

            if (!validateNumber.ValidateString() | !validateRoom.ValidateString() | !validateType.ValidateString()) {
                return;
            } else {
                classModel.setNumber(Integer.parseInt(number.getText().toString()));
                classModel.setClassRoom(room.getText().toString());
                classModel.setDay(day.getText().toString());
                String[] splitHourStart = hourStart.getText().toString().split(" ");
                classModel.setStart(splitHourStart[0]);
                String[] splitHourEnd = hourEnd.getText().toString().split(" ");
                classModel.setEnd(splitHourEnd[0]);
                classModel.setTypeClass(type.getText().toString());

                ApiUpdateClass();
            }
        });

        number.setText(classModel.getNumber().toString());
        room.setText(classModel.getClassRoom());
        day.setText(classModel.getDay());
        hourStart.setText(classModel.getStart() + " H");
        hourEnd.setText(classModel.getEnd() + " H");
        type.setText(classModel.getTypeClass());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void ApiUpdateClass() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.updateClass(classModel.getId(), classModel);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    int res = response.body();

                    if (res > 0) {
                        Toast.makeText(thisContext, R.string.success_updateClass, Toast.LENGTH_SHORT).show();

                        alert.alertExport(thisContext, "Enviar e-mail", "Pretende notificar alguém devido à alteração?", new DialogAlert.Callback() {
                            @Override
                            public void onSucess(int response) {
                                if(response == 1){
                                    Intent goToSendEmail = new Intent(thisContext, PrepareEmail.class);
                                    goToSendEmail.putExtra("subjectEmail", "Alteração de aula");
                                    startActivity(goToSendEmail);
                                    UpdateClass.this.finish();
                                } else {
                                    UpdateClass.this.finish();
                                }

                            }
                        });

                    } else {
                        Toast.makeText(thisContext, R.string.error_updateClass, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        day.setText(dayOfMonth + "/" + (month+1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String auxMinute = minute + "";
        String auxHour = hourOfDay + "";

        if (hourOfDay < 10) {
            auxHour = "0" + auxHour;
        }

        if (minute < 10) {
            auxMinute = "0" + auxMinute;
        }

        if(controllerHour == 0) {
            hourStart.setText(auxHour + ":" + auxMinute + " H");
        } else {
            hourEnd.setText(auxHour + ":" + auxMinute + " H");
        }

    }
}
