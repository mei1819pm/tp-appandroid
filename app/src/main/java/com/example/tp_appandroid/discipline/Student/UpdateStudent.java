package com.example.tp_appandroid.discipline.Student;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.CourseModel;
import com.example.tp_appandroid.models.StudentModel;
import com.example.tp_appandroid.validate.ValidateInput;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateStudent extends AppCompatActivity {

    private StudentModel student;
    private StudentModel studentRequest = new StudentModel();
    private Context thisContext;

    private EditText number, name, email;
    private Button btnUpdate;

    private RadioGroup radioGroup;
    private RadioButton rbDelegate;

    private RadioButton noDelegate, delegate;

    private int valueDelegate;

    private int idDiscipline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_student);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent received = getIntent();
        Gson gson = new Gson();
        student = gson.fromJson(received.getStringExtra("studentUpdate"), StudentModel.class);
        idDiscipline = received.getIntExtra("idDiscipline", 1);

        number = (EditText) findViewById(R.id.number_updateStudent);
        name = (EditText) findViewById(R.id.name_updateStudent);
        email = (EditText) findViewById(R.id.email_updateStudent);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupDelegate_updateStudent);

        noDelegate = (RadioButton) findViewById(R.id.rbStudent_updateStudent);
        delegate = (RadioButton) findViewById(R.id.rbDelegate_updateStudent);

        btnUpdate = (Button) findViewById(R.id.btnUpdateStudent_updateStudent);

        number.setText(student.getNumber()+"");
        name.setText(student.getName());
        email.setText(student.getEmail());

        if(student.getDelegate() == 1) {
            delegate.setChecked(true);
        } else {
            noDelegate.setChecked(true);
        }

        btnUpdate.setOnClickListener(v-> {

            GetDelegate();
            ValidateInput validateNumber =  new ValidateInput(number.getText().toString(), findViewById(R.id.boxNumber_updateStudent));
            ValidateInput validateName =  new ValidateInput(name.getText().toString(), findViewById(R.id.boxName_updateStudent));
            ValidateInput validateEmail =  new ValidateInput(email.getText().toString(), findViewById(R.id.boxEmail_updateStudent));

            if (!validateNumber.ValidateString() | !validateName.ValidateString() | !validateEmail.ValidateString() ) {
                return;

            } else {
                studentRequest.setName(name.getText().toString());
                studentRequest.setNumber(Integer.parseInt(number.getText().toString()));
                studentRequest.setEmail(email.getText().toString());
                studentRequest.setDelegate(valueDelegate);
                studentRequest.setIdDiscipline(idDiscipline);

                apiUpdateStudent();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void GetDelegate() {
        int radioId = radioGroup.getCheckedRadioButtonId();
        rbDelegate = findViewById(radioId);

        switch (rbDelegate.getText().toString()) {
            case "Aluno":
                valueDelegate = 0;
                break;
            case "Delegado":
                valueDelegate = 1;
                break;
            default:
                valueDelegate = 0;
                break;
        }
    }

    private void apiUpdateStudent() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateStudent(student.getId(), studentRequest);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        UpdateStudent.this.finish();
                        Toast.makeText(thisContext, R.string.success_updateStudent, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_updateStudent, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
