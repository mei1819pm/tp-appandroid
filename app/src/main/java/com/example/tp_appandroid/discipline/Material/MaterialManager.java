package com.example.tp_appandroid.discipline.Material;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.discipline.AddDiscipline;
import com.example.tp_appandroid.models.ClassContentModel;
import com.example.tp_appandroid.models.ClassMaterialModel;
import com.example.tp_appandroid.models.StudentModel;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialManager extends AppCompatActivity {

    private Integer idClass;
    private LinearLayout linearLayout;
    private EditText addText;
    private TextView text;
    private Button addBtn, removeBtn;
    private List<ClassMaterialModel> listMaterial;
    private ArrayList<ClassMaterialModel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_manager);

        Intent getIntent = getIntent();
        idClass = getIntent.getIntExtra("class", 1);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Materiais Aula");

        data = new ArrayList<>();

        linearLayout = (LinearLayout) findViewById(R.id.contentManager_parent);
        addText = (EditText) findViewById(R.id.addContent_label);
        addBtn = (Button) findViewById(R.id.addContent_btn);

        addBtn.setOnClickListener(v -> {
            ClassMaterialModel toSave = new ClassMaterialModel();
            toSave.setDescription(addText.getText().toString());
            toSave.setIdClass(idClass);
            ApiSaveClassMaterial(toSave);
            addText.setText("");
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApiGetClassMaterial();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public int onAddField(String s) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.item_content_manager, null);
        text = (TextView) rowView.findViewById(R.id.textcontent);
        removeBtn = (Button) rowView.findViewById(R.id.removeContent_btn);

        text.setText(s);

        removeBtn.setOnClickListener(v -> onDelete(v));

        Integer index = linearLayout.getChildCount() - 1;

        linearLayout.addView(rowView, index);

        return index;
    }

    public void ReloadLayout(List<ClassMaterialModel> list) {
        Integer size = 0;

        if (data != null) size = data.size();

        linearLayout.removeViews(0, size);
        data.clear();

        if(list != null && !list.isEmpty()){
            for (ClassMaterialModel obj : list) {
                Integer index = onAddField(obj.getDescription());
                data.add(index, obj);
            }
        }
    }

    public void onDelete(View v) {
        View parent = (View) v.getParent();
        View view = (View) parent.getParent();
        Integer index = linearLayout.indexOfChild(view);
        ApiDeleteClassMaterial(index);
    }

    private void ApiGetClassMaterial() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<ClassMaterialModel>> call = service.getMaterialsByClass(idClass);
        call.enqueue(new Callback<List<ClassMaterialModel>>() {
            @Override
            public void onResponse(Call<List<ClassMaterialModel>> call, Response<List<ClassMaterialModel>> response) {
                if (response.code() == 200) {
                    listMaterial = response.body();
                    ReloadLayout(listMaterial);
                }
            }

            @Override
            public void onFailure(Call<List<ClassMaterialModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ApiSaveClassMaterial(ClassMaterialModel classMaterialModel) {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> cal = service.addClassMaterial(classMaterialModel);
        cal.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    ApiGetClassMaterial();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void ApiDeleteClassMaterial(Integer position) {
        int idContent = data.get(position).getId();

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.deleteClassMaterial(idContent);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {
                    ApiGetClassMaterial();
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
