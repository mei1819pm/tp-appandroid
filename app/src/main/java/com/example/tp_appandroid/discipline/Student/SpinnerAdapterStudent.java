package com.example.tp_appandroid.discipline.Student;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.tp_appandroid.models.StudentModel;

import java.util.List;

public class SpinnerAdapterStudent extends ArrayAdapter<StudentModel> {


    private Context context;
    private List<StudentModel> values;

    public SpinnerAdapterStudent(Context context, int textViewResourceId, List<StudentModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public StudentModel getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText( values.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        if(position == 0) {
            label.setText(values.get(position).getName());
        } else {
            label.setText(values.get(position).getNumber() + " - " +values.get(position).getName());
        }


        return label;
    }
}
