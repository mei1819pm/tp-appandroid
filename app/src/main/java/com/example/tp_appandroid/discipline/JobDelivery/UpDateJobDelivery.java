package com.example.tp_appandroid.discipline.JobDelivery;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.dialogs.DatePickerFragment;
import com.example.tp_appandroid.dialogs.HourPickerFragment;
import com.example.tp_appandroid.models.JobDeliveryModel;
import com.example.tp_appandroid.validate.ValidateInput;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpDateJobDelivery extends AppCompatActivity implements AdapterView.OnItemSelectedListener,  DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private Context thisContext;

    private JobDeliveryModel jobDelivery;

    private EditText day, hour, description;
    private Spinner spinnerLocation;
    private Button btnUpDateJobDelivery;

    private int positionOption;

    private int startPositionSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_job_delivery);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent received = getIntent();
        Gson gson = new Gson();
        jobDelivery = gson.fromJson(received.getStringExtra("jobDeliveryUpdate"), JobDeliveryModel.class);

        description = (EditText) findViewById(R.id.description_addUpdateJobDelivery);
        day = (EditText) findViewById(R.id.day_addUpdateJobDelivery);
        hour = (EditText) findViewById(R.id.hour_addUpdateJobDelivery);
        spinnerLocation = (Spinner) findViewById(R.id.spinnerLocation_addUpdateJobDelivey);
        btnUpDateJobDelivery = (Button) findViewById(R.id.btnAddUpdateJobDelivery_addUpdateJobDelivery);

        ConstSpinner();

        description.setText(jobDelivery.getDescription());
        day.setText(jobDelivery.getDay());
        hour.setText(jobDelivery.getMaxTime() + " H");
        getStartPositionSpinner();

        day.setOnClickListener(v-> {
            DialogFragment datePicker = new DatePickerFragment();
            datePicker.show(getSupportFragmentManager(), "date picker");
        });

        hour.setOnClickListener(v-> {
            DialogFragment newFragment = new HourPickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        });

        btnUpDateJobDelivery.setOnClickListener(v-> {

            ValidateInput validateDescription =  new ValidateInput(description.getText().toString(), findViewById(R.id.boxDescription_addUpdateJobDelivery));

            if (!validateDescription.ValidateString()) {
                return;

            } else if(positionOption == 0){
                Toast.makeText(thisContext, R.string.error_spinnerJobDelivery, Toast.LENGTH_SHORT).show();

            } else {
                jobDelivery.setDescription(description.getText().toString());
                jobDelivery.setDay(day.getText().toString());
                String[] splitHour = hour.getText().toString().split(" ");
                jobDelivery.setMaxTime(splitHour[0]);

                apiUpdateStudent();
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (positionOption == 0) {
            parent.setSelection(startPositionSpinner);
            positionOption = 1;
        }

        jobDelivery.setDeliveryLocation(parent.getSelectedItem().toString());
        positionOption = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void ConstSpinner() {
        ArrayAdapter<CharSequence> adapterDayWeek = ArrayAdapter.createFromResource(thisContext,
                R.array.location_jobDelivery, android.R.layout.simple_spinner_item);
        adapterDayWeek.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocation.setAdapter(adapterDayWeek);
        spinnerLocation.setOnItemSelectedListener(this);
    }

    private void getStartPositionSpinner() {
        List<String> arraySpinner = Arrays.asList(getResources().getStringArray(R.array.location_jobDelivery));

        for (String aux : arraySpinner) {
            if (aux.equals(jobDelivery.getDeliveryLocation())) {
                startPositionSpinner = arraySpinner.indexOf(aux);
                break;
            }
        }
    }

    private void apiUpdateStudent() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.updateJobDelivery(jobDelivery.getId(), jobDelivery);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        UpDateJobDelivery.this.finish();
                        Toast.makeText(thisContext, R.string.success_updateJobDelivery, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_updateJobDelivery, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        day.setText(dayOfMonth + "/" + (month+1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String auxMinute = minute + "";
        String auxHour = hourOfDay + "";

        if (hourOfDay < 10) {
            auxHour = "0" + auxHour;
        }

        if (minute < 10) {
            auxMinute = "0" + auxMinute;
        }

        hour.setText(auxHour + ":" + auxMinute + " H");
    }
}
