package com.example.tp_appandroid.discipline.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.planification.Discipline;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentAttendanceDiscipline extends Fragment {

    private TextView day, hour;
    private View v;

    private int idDiscipline = 0;

    private String[] timeAttendace;

    public FragmentAttendanceDiscipline() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_fragment_attendance_discipline, container, false);

        day = (TextView) v.findViewById(R.id.day_attendaceDiscipline);
        hour = (TextView) v.findViewById(R.id.hour_attendaceDiscipline);

        GetIdDiscipline();
        apiGetTimeAttendace();

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void GetIdDiscipline(){
        Discipline activity = (Discipline) getActivity();
        idDiscipline = activity.GetIdDiscipline();
    }

    private void apiGetTimeAttendace() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<String[]> call = service.getTimeAttendace(idDiscipline);
        call.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.code() == 200) {
                    timeAttendace = response.body();

                    day.setText(timeAttendace[0]);
                    hour.setText(timeAttendace[1]);
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

}
