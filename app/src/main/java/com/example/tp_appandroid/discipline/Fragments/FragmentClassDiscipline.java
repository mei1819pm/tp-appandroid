package com.example.tp_appandroid.discipline.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.adapters.AdapterRecycler.RecyclerViewAdapterClass;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.discipline.Class.AddClass;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.planification.Discipline;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentClassDiscipline extends Fragment {

    private View v;
    public FragmentClassDiscipline() {
        // Required empty public constructor
    }

    private RecyclerView recyclerViewClass;
    private List<ClassModel> listClass = new ArrayList<>();

    private int idDiscipline = 0;

    private int showMassage = 1;

    private FloatingActionButton btnAdd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_fragment_class_discipline, container, false);

        if(showMassage == 0) {
            GoneMessage();
        }

        ConstRecyclerView();

        btnAdd = (FloatingActionButton) v.findViewById(R.id.btnAdd_classDiscipline);

        btnAdd.setOnClickListener(v-> {
            Intent goToAddClass = new Intent(getContext(), AddClass.class);
            goToAddClass.putExtra("idDiscipline", idDiscipline);
            startActivity(goToAddClass);
        });

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GetIdDiscipline();
        apiGetClasses();
    }

    @Override
    public void onResume() {
        super.onResume();
        apiGetClasses();
    }

    private void ConstRecyclerView() {
        recyclerViewClass = (RecyclerView) v.findViewById(R.id.recyclerview_classDiscipline);
        RecyclerViewAdapterClass adapter = new RecyclerViewAdapterClass(getContext(), listClass, idDiscipline);
        recyclerViewClass.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewClass.setAdapter(adapter);

        adapter.setOnItemClickListener(new RecyclerViewAdapterClass.ClickListener() {
            @Override
            public void onItemClick(View v, int position) {

            }

            @Override
            public void onItemLongClick(View v, int position) {

            }

        });
    }

    private void GetIdDiscipline(){
        Discipline activity = (Discipline) getActivity();
        idDiscipline = activity.GetIdDiscipline();
    }

    private void apiGetClasses() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<ClassModel>> call = service.getClassByDiscipline(idDiscipline);
        call.enqueue(new Callback<List<ClassModel>>() {
            @Override
            public void onResponse(Call<List<ClassModel>> call, Response<List<ClassModel>> response) {
                if (response.code() == 200) {
                    if (response.body().size() > 0) {
                        listClass = response.body();
                        ConstRecyclerView();
                        showMassage = 0;
                        GoneMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ClassModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void GoneMessage() {
        TextView empty = v.findViewById(R.id.classEmpty_classDiscpline);
        empty.setVisibility(View.GONE);
    }
}
