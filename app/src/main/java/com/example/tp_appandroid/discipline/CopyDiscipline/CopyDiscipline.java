package com.example.tp_appandroid.discipline.CopyDiscipline;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp_appandroid.R;
import com.example.tp_appandroid.adapters.AdapterSpinner.SpinnerAdapterCourse;
import com.example.tp_appandroid.adapters.AdapterSpinner.SpinnerAdapterDiscipline;
import com.example.tp_appandroid.adapters.AdapterSpinner.SpinnerAdapterSchoolYear;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.CourseModel;
import com.example.tp_appandroid.models.DisciplineModel;
import com.example.tp_appandroid.models.SchoolYearModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CopyDiscipline extends AppCompatActivity {

    private Context thisContext;

    private SharedPreferences userSession;
    private int idUser;

    private int idDisciplineFrom;

    private List<SchoolYearModel> listSchoolYear = new ArrayList<>();
    private Spinner spinnerSchoolYear;
    private SpinnerAdapterSchoolYear adapterSchoolYear;
    private int idSchoolYear;
    private TextView titleSchoolYear;

    private List<CourseModel> listCourse = new ArrayList<>();
    private Spinner spinnerCourse;
    private SpinnerAdapterCourse adapterCourse;
    private int idCourse;
    private TextView titleCourse;

    private List<DisciplineModel> listDiscipline = new ArrayList<>();
    private Spinner spinnerDiscipline;
    private SpinnerAdapterDiscipline adapterDiscipline;
    private int idDisciplineTo;
    private TextView titleDiscipline;

    private Button btnCopyDiscipline;

    private ProgressBar loadingCopyDiscipline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copy_discipline);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisContext = this;

        Intent received = getIntent();
        idDisciplineFrom = received.getIntExtra("discipline",1);

        userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
        idUser = userSession.getInt("idUser", 0);

        spinnerSchoolYear = (Spinner) findViewById(R.id.spinnerSchoolYear_copyDiscipline);
        spinnerCourse = (Spinner) findViewById(R.id.spinnerCourse_copyDiscipline);
        spinnerDiscipline = (Spinner) findViewById(R.id.spinnerDiscipline_copyDiscipline);
        btnCopyDiscipline = (Button) findViewById(R.id.btnCopy_copyDiscipline);

        titleSchoolYear = (TextView) findViewById(R.id.titleSchoolYear_copyDiscipline);
        titleCourse = (TextView) findViewById(R.id.titleCourse_copyDiscipline);
        titleDiscipline = (TextView) findViewById(R.id.titleDiscipline_copyDiscipline);

        loadingCopyDiscipline = (ProgressBar) findViewById(R.id.loading_copyDiscipline);

        listSchoolYear.add(new SchoolYearModel( "Selecione o ano letivo"));

        ConstSpinnerSchoolYear();
        ConstSpinnerCourse();
        ConstSpinnerDiscipline();

        apiGetSchoolYears();

        btnCopyDiscipline.setOnClickListener(v-> {
            apiCopyDiscipline();
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void ConstSpinnerSchoolYear() {
        adapterSchoolYear = new SpinnerAdapterSchoolYear(thisContext, android.R.layout.simple_list_item_1, listSchoolYear);

        spinnerSchoolYear.setAdapter(adapterSchoolYear);
        spinnerSchoolYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                SchoolYearModel schoolYear = adapterSchoolYear.getItem(position);
                idSchoolYear = schoolYear.getId();

                if(position == 0) {
                    titleCourse.setVisibility(View.GONE);
                    spinnerCourse.setVisibility(View.GONE);
                    titleDiscipline.setVisibility(View.GONE);
                    spinnerDiscipline.setVisibility(View.GONE);
                    btnCopyDiscipline.setVisibility(View.GONE);
                } else {
                    titleCourse.setVisibility(View.VISIBLE);
                    spinnerCourse.setVisibility(View.VISIBLE);
                    apiGetCourse();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void ConstSpinnerCourse() {
        adapterCourse = new SpinnerAdapterCourse(thisContext, android.R.layout.simple_list_item_1, listCourse);

        spinnerCourse.setAdapter(adapterCourse);
        spinnerCourse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                CourseModel course = adapterCourse.getItem(position);
                idCourse = course.getId();

                if(position == 0) {
                    titleDiscipline.setVisibility(View.GONE);
                    spinnerDiscipline.setVisibility(View.GONE);
                } else {
                    titleDiscipline.setVisibility(View.VISIBLE);
                    spinnerDiscipline.setVisibility(View.VISIBLE);
                    apiGetDiscipline();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void ConstSpinnerDiscipline() {
        adapterDiscipline = new SpinnerAdapterDiscipline(thisContext, android.R.layout.simple_list_item_1, listDiscipline);

        spinnerDiscipline.setAdapter(adapterDiscipline);
        spinnerDiscipline.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                DisciplineModel discipline = adapterDiscipline.getItem(position);
                idDisciplineTo = discipline.getId();

                if (position == 0) {
                    btnCopyDiscipline.setVisibility(View.GONE);
                }
                else {
                    btnCopyDiscipline.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void apiGetSchoolYears() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<SchoolYearModel>> call = service.getAllScoolyears();
        call.enqueue(new Callback<List<SchoolYearModel>>() {
            @Override
            public void onResponse(Call<List<SchoolYearModel>> call, Response<List<SchoolYearModel>> response) {
                if (response.code() == 200) {

                    if (response.body().size() > 0) {
                        listSchoolYear.addAll(response.body());
                        ConstSpinnerSchoolYear();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<SchoolYearModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void apiGetCourse() {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<CourseModel>> call = service.getCourseByUserSchoolYear(idUser, idSchoolYear);
        call.enqueue(new Callback<List<CourseModel>>() {
            @Override
            public void onResponse(Call<List<CourseModel>> call, Response<List<CourseModel>> response) {
                if (response.code() == 200) {


                    if (response.body().size() > 0) {
                        listCourse.clear();
                        listCourse.add(new CourseModel("Selecione o curso"));
                        listCourse.addAll(response.body());
                        ConstSpinnerCourse();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CourseModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void apiGetDiscipline() {
        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<DisciplineModel>> call = service.getDisciplineByCourse(idUser, idCourse);
        call.enqueue(new Callback<List<DisciplineModel>>() {

            @Override
            public void onResponse(Call<List<DisciplineModel>> call, Response<List<DisciplineModel>> response) {
                if (response.code() == 200 && response.body().size() > 0) {
                    listDiscipline.clear();
                    listDiscipline.add(new DisciplineModel("Selecione a disciplina"));
                    listDiscipline.addAll(response.body());
                    ConstSpinnerDiscipline();
                }
            }

            @Override
            public void onFailure(Call<List<DisciplineModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void apiCopyDiscipline() {
        loadingCopyDiscipline.setVisibility(View.VISIBLE);

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.copyDiscipline(idDisciplineFrom, idDisciplineTo);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.code() == 200) {

                    if (response.body() == 1) {
                        CopyDiscipline.this.finish();
                        Toast.makeText(thisContext, R.string.success_copyDisciplina, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(thisContext, R.string.error_copyDisicpline, Toast.LENGTH_SHORT).show();
                        loadingCopyDiscipline.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
