package com.example.tp_appandroid.service_notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.example.tp_appandroid.today.HomeApp;
import com.example.tp_appandroid.R;

public class NotificationCreator {

    Context thisContext;

    public NotificationCreator(Context context) {
        this.thisContext = context;
    }

    public void createNotificationContactHour(int id, String title, String discipline, String hour, String roomOrMethod, String day, String student) {
        //Notificações
        NotificationManager notificationManager = (NotificationManager) thisContext.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(thisContext, 0, new Intent(thisContext, HomeApp.class), 0); // AO clicar na notificar ele abre a paginia dois

        NotificationCompat.Builder builder = new NotificationCompat.Builder(thisContext); // Channel Id a mudar para ter varias notificações
        //Ecra Bloqueado
        //builder.setVisibility(VISIBILITY_PRIVATE);

        builder.setSmallIcon(R.drawable.icon_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_notification));

        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setTicker(title);
        builder.setContentTitle(title);
        builder.setContentText("Expandir notificação para ver detalhes"); //Pra texto pequeno
        builder.setStyle(new NotificationCompat.InboxStyle()
                .addLine("Hora: " + hour)
                .addLine("Data: " + day)
                .addLine("Local: " + roomOrMethod)
                .addLine("Aluno: " + student)
                .addLine("Disciplina: " + discipline)
                .setBigContentTitle(title));

        // led
        builder.setLights(0x0000FF, 4000, 4000);

        builder.setContentIntent(pendingIntent); // Ativar mudar de atividade ao clicar

        android.app.Notification notification = builder.build();
        notification.vibrate = new long[]{150, 300, 150, 600}; //vibrar, espera 150, vibra 300, espera 150, vibra 600 milsegundos

        notificationManager.notify(id, notification);

        try {
            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); // Som com Notificação
            Ringtone toque = RingtoneManager.getRingtone(thisContext, som);
            toque.play();
        } catch (Exception e) {

        }
    }


    public void createNotificationClassJob(int id, String title, String discipline, String hour, String roomOrMethod, String day) {
        //Notificações
        NotificationManager notificationManager = (NotificationManager) thisContext.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(thisContext, 0, new Intent(thisContext, HomeApp.class), 0); // AO clicar na notificar ele abre a paginia dois

        NotificationCompat.Builder builder = new NotificationCompat.Builder(thisContext); // Channel Id a mudar para ter varias notificações
        //Ecra Bloqueado
        //builder.setVisibility(VISIBILITY_PRIVATE);

        builder.setSmallIcon(R.drawable.icon_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(thisContext.getResources(), R.drawable.icon_notification));

        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setTicker(title);
        builder.setContentTitle(title);
        builder.setContentText("Expandir notificação para ver detalhes"); //Pra texto pequeno
        builder.setStyle(new NotificationCompat.InboxStyle()
                .addLine("Hora: " + hour)
                .addLine("Data: " + day)
                .addLine("Local: " + roomOrMethod)
                .addLine("Disciplina: " + discipline)
                .setBigContentTitle(title));

        // led
        builder.setLights(0x0000FF, 4000, 4000);

        builder.setContentIntent(pendingIntent); // Ativar mudar de atividade ao clicar

        android.app.Notification notification = builder.build();
        notification.vibrate = new long[]{150, 300, 150, 600}; //vibrar, espera 150, vibra 300, espera 150, vibra 600 milsegundos

        notificationManager.notify(id, notification);

        try {
            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); // Som com Notificação
            Ringtone toque = RingtoneManager.getRingtone(thisContext, som);
            toque.play();
        } catch (Exception e) {

        }
    }
}
