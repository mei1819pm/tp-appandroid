package com.example.tp_appandroid.service_notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class NotificationBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        String message = "Ligar Serviço Agenda do Professor";

        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

        if(Intent.ACTION_BOOT_COMPLETED.equals(action) || action.equals("START_SERVICE_SEND_NOTIFICATION"))
        {

            callServiceSendNotification(context);
        }
    }

    public void callServiceSendNotification(Context context) {

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        // Create intent to invoke the background service.
        Intent intent = new Intent(context, NotificationService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long startTime = System.currentTimeMillis();
        long intervalTime = 5000;

        // Create repeat alarm.
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startTime, intervalTime, pendingIntent);
    }
}
