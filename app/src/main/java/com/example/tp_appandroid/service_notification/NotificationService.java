package com.example.tp_appandroid.service_notification;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import com.example.tp_appandroid.other_class.CheckConnection;
import com.example.tp_appandroid.api.IApiApp;
import com.example.tp_appandroid.models.NotificationModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;


public class NotificationService extends Service {

    NotificationCreator notification;
    private boolean isRunning;
    Context context;
    private Thread backgroundThread;

    private CheckConnection checkConnection;

    private SharedPreferences userSession;
    private int idUser;

    private int notificationStatus = -1;
    private List<NotificationModel> notificationList = new ArrayList<>();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        this.context = this;
        this.isRunning = false;
        this.backgroundThread = new Thread(myTask);

    }

    private Runnable myTask = new Runnable() {
        public void run() {

            checkConnection = new CheckConnection(context);

            if (checkConnection.isConnected()) {

                userSession = getSharedPreferences("userSession_app", MODE_PRIVATE);
                idUser = userSession.getInt("idUser", 0);

                try {
                    notificationStatus = getNotificationStatus(idUser);

                    if (notificationStatus == 1) {
                        notificationList = getNotifications(idUser);

                        notification = new NotificationCreator(getApplicationContext());

                        for (NotificationModel auxNotification : notificationList) {
                            if(auxNotification.getTitle().equals("Apoio")) {
                                notification.createNotificationContactHour(auxNotification.getId(), auxNotification.getTitle(), auxNotification.getDiscipline(), auxNotification.getHour(), auxNotification.getRoomOrMethod(), auxNotification.getDay(), auxNotification.getStudent());

                            } else {
                                notification.createNotificationClassJob(auxNotification.getId(), auxNotification.getTitle(), auxNotification.getDiscipline(), auxNotification.getHour(), auxNotification.getRoomOrMethod(), auxNotification.getDay());

                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            stopSelf();
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!this.isRunning) {
            this.isRunning = true;
            this.backgroundThread.start();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        this.isRunning = false;
    }

    private Integer getNotificationStatus(int auxIdUser) throws IOException {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<Integer> call = service.getNotificationStatusById(auxIdUser);
        return call.execute().body();

    }

    private List<NotificationModel> getNotifications(int auxIdUser) throws IOException {

        IApiApp service = IApiApp.retrofi.create(IApiApp.class);

        Call<List<NotificationModel>> call = service.getNotificationById(auxIdUser);
        return call.execute().body();

    }
}
