package com.example.tp_appandroid.dialogs;

import android.content.Context;
import android.content.DialogInterface;

public class DialogAlert {

    public void alertExport(Context thisContext, String title, String message, final Callback callback) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                thisContext);

        alertDialogBuilder.setTitle(title);

        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Sim",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        callback.onSucess(1);
                    }
                })
                .setNegativeButton("Não",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        callback.onSucess(0);
                    }
                });

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public interface Callback {

        public void onSucess(int t);

    }
}
