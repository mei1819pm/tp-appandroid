package com.example.tp_appandroid.api;

import com.example.tp_appandroid.models.ClassContentModel;
import com.example.tp_appandroid.models.ClassMaterialModel;
import com.example.tp_appandroid.models.ClassModel;
import com.example.tp_appandroid.models.ContactHourModel;
import com.example.tp_appandroid.models.CourseModel;
import com.example.tp_appandroid.models.DisciplineModel;
import com.example.tp_appandroid.models.EmailModel;
import com.example.tp_appandroid.models.EventCalendarModel;
import com.example.tp_appandroid.models.JobDeliveryModel;
import com.example.tp_appandroid.models.NotificationModel;
import com.example.tp_appandroid.models.SchoolYearModel;
import com.example.tp_appandroid.models.StudentModel;
import com.example.tp_appandroid.models.TeacherModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IApiApp {

    String HOST = "http://213.22.111.242:4200/api/";

    @GET("getClassToday/{idUser}")
    Call<List<ClassModel>> getClassToday(
            @Path("idUser") int idUser
    );

    @GET("getContactToday/{idUser}")
    Call<List<ContactHourModel>> getContactToday(
            @Path("idUser") int idUser
    );

    @GET("getJobToday/{idUser}")
    Call<List<JobDeliveryModel>> getJobBeliveryToday(
            @Path("idUser") int idUser
    );

    @GET("getUserById/{idUser}")
    Call<TeacherModel> getUserById(
            @Path("idUser") int idUser
    );

    @GET("getNotificationStatusById/{idUser}")
    Call<Integer> getNotificationStatusById(
            @Path("idUser") int idUser
    );

    @GET("getNotificationById/{idUser}")
    Call<List<NotificationModel>> getNotificationById(
            @Path("idUser") int idUser
    );

    @GET("getAllSchoolYears")
    Call<List<SchoolYearModel>> getAllScoolyears();

    @GET("getAllEventsCalendar/{idUser}")
    Call<List<EventCalendarModel>> getAllEventsTwoYears(
            @Path("idUser") int idUser
    );

    @GET("getClassByDay/{idUser}/{date}")
    Call<List<ClassModel>> getClassByDay(
            @Path("idUser") int idUser,
            @Path("date") String date
    );

    @GET("getContactHourByDay/{idUser}/{date}")
    Call<List<ContactHourModel>> getContactHourByDay(
            @Path("idUser") int idUser,
            @Path("date") String date
    );

    @GET("getJobDeliveryByDay/{idUser}/{date}")
    Call<List<JobDeliveryModel>> getJobDeliveryByDay(
            @Path("idUser") int idUser,
            @Path("date") String date
    );

    @GET("getPlanification")
    Call<List<DisciplineModel>> getPlanification(
            @Query("idUser") int idUser,
            @Query("idSchoolYear") int idSchoolYear,
            @Query("idDiscipline") int idDiscipline
    );

    @GET("getCourseByUserSchoolYear/{idUser}/{idSchoolYear}")
    Call<List<CourseModel>> getCourseByUserSchoolYear(
            @Path("idUser") int idUser,
            @Path("idSchoolYear") int idSchoolYear
    );

    @GET("getDisciplinesByCourse/{idUser}/{idCourse}")
    Call<List<DisciplineModel>> getDisciplineByCourse(
            @Path("idUser") int idUser,
            @Path("idCourse") int idSchoolYear
    );

    @GET("getStudentByDiscipline/{idDiscipline}")
    Call<List<StudentModel>> getStudentByDiscipline(
            @Path("idDiscipline") int idDiscipline
    );

    @GET("getStudentInsert/{idUser}")
    Call<List<StudentModel>> getStudentInsert(
            @Path("idUser") int idUser
    );

    @GET("getTimeAttandace/{idDiscipline}")
    Call<String[]> getTimeAttendace(
            @Path("idDiscipline") int idDiscipline
    );

    @GET("getJobDeliveryByDiscipline/{idDiscipline}")
    Call<List<JobDeliveryModel>> getJobDeliveryByDiscipline(
            @Path("idDiscipline") int idDiscipline
    );

    @GET("getContactHourByDiscipline/{idDiscipline}")
    Call<List<ContactHourModel>> getContactHourByDiscipline(
            @Path("idDiscipline") int idDiscipline
    );

    @GET("getClassByDiscipline/{idDiscipline}")
    Call<List<ClassModel>> getClassByDiscipline(
            @Path("idDiscipline") int idDiscipline
    );

    @GET("getContentsByClass/{idClass}")
    Call<List<ClassContentModel>> getContentsByClass(
            @Path("idClass") int idClass
    );

    @GET("getMaterialsByClass/{idClass}")
    Call<List<ClassMaterialModel>> getMaterialsByClass(
            @Path("idClass") int idClass
    );

    @GET("getCurrentLessonNumberForDiscipline/{idDiscipline}")
    Call<Integer> getCurrentLessonNumberForDiscipline(
            @Path("idDiscipline") int idDiscipline
    );
    //POSTS

    @POST("checkLogin")
     Call<TeacherModel> checkLogin(
             @Body TeacherModel teacherRequest
    );

    @POST("createAccount")
    Call<Integer> createAccount(
            @Body TeacherModel teacherRequest
    );

    @POST("sendEmail")
    Call<Integer> sendEmail(
            @Body EmailModel dataEmail
    );

    @POST("sendEmailAdministrative")
    Call<Integer> sendEmailAdministrative(
            @Body EmailModel dataEmail
    );

    @POST("addCourse")
    Call<Integer> addCourse(
            @Body DisciplineModel discipline
    );

    @POST("addDiscipline")
    Call<Integer> addDiscipline(
            @Body DisciplineModel discipline
    );

    @POST("addStudentDiscipline")
    Call<Integer> addStudentDiscipline(
            @Body StudentModel student
    );

    @POST("addStudent")
    Call<Integer> addStudent(
            @Body StudentModel student
    );

    @POST("addJobDelivery")
    Call<Integer> addJobDelivery(
            @Body JobDeliveryModel jobDelivery
    );

    @POST("addContactHour")
    Call<Integer> addContactHour(
            @Body ContactHourModel contactHour
    );

    @POST("addClass")
    Call<Integer> addClass(
            @Body ClassModel classModel
    );

    @POST("addClassContent")
    Call<Integer> addClassContent(
            @Body ClassContentModel classContentModel
    );

    @POST("addClassMaterial")
    Call<Integer> addClassMaterial(
            @Body ClassMaterialModel ClassMaterialModel
    );

    // PUTS

    @PUT("updateUserStatus/{idUser}")
    Call<Integer> deleteUser(
            @Path("idUser") int idUser
    );

    @PUT("updatePersonalData/{idUser}")
    Call<Integer> updatePersonalData(
            @Path("idUser") int idUser,
            @Body TeacherModel teacherRequest
    );

    @PUT("updateNotification/{idUser}")
    Call<Integer> updateNotification(
            @Path("idUser") int idUser,
            @Body TeacherModel teacherRequest
    );

    @PUT("updateAcademicFormation/{idUser}")
    Call<Integer> updateAccedemicFormation(
            @Path("idUser") int idUser,
            @Body TeacherModel teacherRequest
    );

    @PUT("updateWorkplace/{idUser}")
    Call<Integer> updateWorkPlace(
            @Path("idUser") int idUser,
            @Body TeacherModel teacherRequest
    );

    @PUT("updateCourse/{idCourse}")
    Call<Integer> updateCourse(
            @Path("idCourse") int idCourse,
            @Body CourseModel course
    );

    @PUT("updateDiscipline/{id}")
    Call<Integer> updateDiscipline(
            @Path("id") int idDiscipline,
            @Body DisciplineModel disciplineModel
    );

    @PUT("updateClass/{idClass}")
    Call<Integer> updateClass(
            @Path("idClass") int idClass,
            @Body ClassModel lesson
    );

    @PUT("updateStudent/{idStudent}")
    Call<Integer> updateStudent(
            @Path("idStudent") int idStudent,
            @Body StudentModel student
    );

    @PUT("updateJobDelivery/{idJobDelivery}")
    Call<Integer> updateJobDelivery(
            @Path("idJobDelivery") int idJobDelivery,
            @Body JobDeliveryModel jobDelivery
    );

    @PUT("updateContactHour/{idContactHour}")
    Call<Integer> updateContactHour(
            @Path("idContactHour") int idContactHour,
            @Body ContactHourModel contactHour
    );

    @PUT("copyDiscipline/{idDisciplineFrom}/{idDisciplineTo}")
    Call<Integer> copyDiscipline(
            @Path("idDisciplineFrom") int idDisciplineFrom,
            @Path("idDisciplineTo") int idDisciplineTo
    );

    @DELETE("deleteCourse/{idCourse}")
    Call<Integer> deleteCourse(
            @Path("idCourse") int idCourse
    );

    @DELETE("deleteDiscipline/{idDiscipline}")
    Call<Integer> deleteDiscipline(
            @Path("idDiscipline") int idDiscipline
    );

    @DELETE("deleteStudent/{idDiscipline}/{idStudent}")
    Call<Integer> deleteStudent(
            @Path("idDiscipline") int idDiscipline,
            @Path("idStudent") int idStudent
    );

    @DELETE("deleteJobDelivery/{idJobDelivery}")
    Call<Integer> deleteJobDelivery(
            @Path("idJobDelivery") int idJobDelivery
    );

    @DELETE("deleteContactHour/{idContactHour}")
    Call<Integer> deleteContactHour(
            @Path("idContactHour") int idContactHour
    );

    @DELETE("deleteClass/{idClass}")
    Call<Integer> deleteClass(
            @Path("idClass") int idClass
    );

    @DELETE("deleteClassContent/{idClassContent}")
    Call<Integer> deleteClassContent(
            @Path("idClassContent") int idClassContent
    );

    @DELETE("deleteClassMaterial/{idClassMaterial}")
    Call<Integer> deleteClassMaterial(
            @Path("idClassMaterial") int idClassMaterial
    );

    Retrofit retrofi = new Retrofit.Builder()
            .baseUrl(HOST)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
